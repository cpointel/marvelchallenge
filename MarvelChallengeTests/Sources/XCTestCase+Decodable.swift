//
//  XCTestCase+Decodable.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 22/10/2020.
//

import Foundation
import XCTest

extension XCTestCase {
    func getObject<T: Decodable>(fromFile fileName: String) -> T? {
        let bundle = Bundle(for: type(of: self))
        return Helper.getObject(fromFile: fileName, bundle: bundle)
    }

    func getObjects<T: Decodable>(fromFile fileName: String) -> [T]? {
        let bundle = Bundle(for: type(of: self))
        return Helper.getObjects(fromFile: fileName, bundle: bundle)
    }
}

private class Helper {
    static func loadFixtureData(bundle: Bundle, nameFile: String, extensionFile: String) -> Data? {

        guard let jsonUrl = bundle.url(forResource: nameFile, withExtension: extensionFile) else {
            XCTFail("Json file \(nameFile).\(extensionFile) not found")
            return nil
        }

        guard let data = try? Data(contentsOf: jsonUrl) else {
            XCTFail("Json file not data")
            return nil
        }

        return data
    }

    static func getObject<T: Decodable>(fromFile fileName: String, bundle: Bundle) -> T? {
        if let data = loadFixtureData(bundle: bundle, nameFile: fileName, extensionFile: "json") {
            do {
                return try JSONDecoder().decode(T.self, from: data)
            } catch {
                print(error)
            }
        }
        return nil
    }

    static func getObjects<T: Decodable>(fromFile fileName: String, bundle: Bundle) -> [T]? {
        if let data = loadFixtureData(bundle: bundle, nameFile: fileName, extensionFile: "json") {
            do {
                return try JSONDecoder().decode([T].self, from: data)
            } catch {
                print(error)
            }
        }
        return nil
    }
}
