//
//  RxXCTestCase.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 22/10/2020.
//

import XCTest
import RxTest
import RxSwift

class RxXCTestCase: XCTestCase {

    // MARK: - Properties
    private(set) var scheduler: TestScheduler!
    private(set) var disposeBag = DisposeBag()

    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
        scheduler = TestScheduler(initialClock: 0)
    }

    override func tearDown() {
        disposeBag = DisposeBag()
        super.tearDown()
    }
}
