//
//  APIComicDTOMock.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 27/10/2020.
//

import Foundation
@testable import MarvelChallenge

extension APIComicDTO {

    static func mock(id: Int = 100,
                     title: String = "MOCK_TITLE",
                     description: String = "MOCK_DESCRIPTION",
                     thumbnail: APIThumbnailDTO = APIThumbnailDTO.mock()) -> APIComicDTO {
        return APIComicDTO(id: id,
                           title: title,
                           description: description,
                           thumbnail: thumbnail)
    }
}
