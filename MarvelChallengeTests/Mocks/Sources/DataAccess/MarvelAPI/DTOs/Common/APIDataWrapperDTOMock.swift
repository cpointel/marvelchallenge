//
//  APIDataWrapperDTOMOck.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import Foundation
@testable import MarvelChallenge

extension APIDataWrapperDTO {

    static func mock(code: Int = 200,
                     data: APIDataContainerDTO<T> = APIDataContainerDTO.mock()) -> APIDataWrapperDTO {
        return APIDataWrapperDTO(code: code,
                                 data: data)
    }
}
