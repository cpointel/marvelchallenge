//
//  APIDataContainerDTOMock.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import Foundation
@testable import MarvelChallenge

extension APIDataContainerDTO {

    static func mock(offset: Int = 10,
                     limit: Int = 5,
                     total: Int = 100,
                     count: Int = 7,
                     results: [T] = []) -> APIDataContainerDTO {
        return APIDataContainerDTO(offset: offset,
                                   limit: limit,
                                   total: total,
                                   count: count,
                                   results: results)
    }
}
