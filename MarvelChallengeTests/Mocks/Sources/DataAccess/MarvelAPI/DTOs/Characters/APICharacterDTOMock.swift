//
//  APICharacterDTOMock.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import Foundation
@testable import MarvelChallenge

extension APICharacterDTO {

    static func mock(id: Int = 100,
                     name: String = "MOCK_NAME",
                     description: String = "MOCK_DESCRIPTION",
                     thumbnail: APIThumbnailDTO = APIThumbnailDTO.mock()) -> APICharacterDTO {
        return APICharacterDTO(id: id,
                               name: name,
                               description: description,
                               thumbnail: thumbnail)
    }
}
