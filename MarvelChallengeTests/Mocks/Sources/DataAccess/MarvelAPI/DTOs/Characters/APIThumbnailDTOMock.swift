//
//  APIThumbnailDTOMock.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import Foundation
@testable import MarvelChallenge

extension APIThumbnailDTO {

    static func mock(path: String = "MOCK_PATH",
                     ext: String = "MOCK_EXT") -> APIThumbnailDTO {
        return APIThumbnailDTO(path: path,
                               ext: ext)
    }
}
