//
//  MarvelApiMock.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import Foundation
@testable import MarvelChallenge
import RxSwift

class MarvelApiMock: MarvelApiProtocol {

    // MARK: - MockProperties
    var mock_fetchCharactersCalled: ((_ dto: APICharactersRequestDTO) -> Single<APIDataWrapperDTO<APICharacterDTO>>)?
    var mock_fetchComicsCalled: ((_ for: Int, _ dto: APIComicsRequestDTO) -> Single<APIDataWrapperDTO<APIComicDTO>>)?

    // MARK: - MarvelApiProtocol / Functions
    func fetchCharacters(dto: APICharactersRequestDTO) -> Single<APIDataWrapperDTO<APICharacterDTO>> {
        return mock_fetchCharactersCalled?(dto) ?? Single.never()
    }

    func fetchComics(for characterId: Int, dto: APIComicsRequestDTO) -> Single<APIDataWrapperDTO<APIComicDTO>> {
        return mock_fetchComicsCalled?(characterId, dto) ?? Single.never()
    }
}
