//
//  CharactersListRoutingMock.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 25/10/2020.
//

import Foundation
@testable import MarvelChallenge

class CharactersListRoutingMock: CharactersListRouting {

    // MARK: - MockProperties
    var mock_showCharacterCalled: ((_ character: Character) -> Void)?

    // MARK: - CharactersListRouting / Functions
    func showCharacter(_ character: Character) {
        mock_showCharacterCalled?(character)
    }
}
