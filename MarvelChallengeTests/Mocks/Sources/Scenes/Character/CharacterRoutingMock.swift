//
//  CharacterRoutingMock.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 27/10/2020.
//

import Foundation
@testable import MarvelChallenge

class CharacterRoutingMock: CharacterRouting {

    // MARK: - MockProperties
    var mock_backCalled: (() -> Void)?

    // MARK: - CharacterRouting / Functions
    func back() {
        mock_backCalled?()
    }
}
