//
//  BusinessServicesMock.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import Foundation
@testable import MarvelChallenge

class BusinessServicesMock: BusinessServicesProtocol {

    // MARK: - BusinessServicesProtocol / Properties
    var charactersService: CharactersServiceProtocol = CharactersServiceMock()
}
