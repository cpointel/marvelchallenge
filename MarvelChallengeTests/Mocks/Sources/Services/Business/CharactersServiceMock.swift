//
//  CharactersServiceMock.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import Foundation
@testable import MarvelChallenge
import RxSwift

class CharactersServiceMock: CharactersServiceProtocol {

    // MARK: - MockProperties
    var mock_charactersCalled: ((_ nameStartsWith: String?, _ offset: Int, _ limit: Int) -> Single<CharactersList>)?
    var mock_comicsCalled: ((_ characterId: Int) -> Single<ComicsList>)?

    // MARK: - MarvelApiProtocol / Functions
    func characters(nameStartsWith: String?, atOffset offset: Int, limit: Int) -> Single<CharactersList> {
        return mock_charactersCalled?(nameStartsWith, offset, limit) ?? Single.never()
    }

    func comics(for characterId: Int) -> Single<ComicsList> {
        return mock_comicsCalled?(characterId) ?? Single.never()
    }
}
