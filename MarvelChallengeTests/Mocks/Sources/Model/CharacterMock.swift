//
//  CharacterMock.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import Foundation
@testable import MarvelChallenge

extension Character {

    static func mock(id: Int = 0,
                     name: String = "MOCK_NAME",
                     description: String = "MOCK_DESCRIPTION",
                     thumbnail: Thumbnail = Thumbnail.mock()) -> Character {
        return Character(id: id,
                         name: name,
                         description: description,
                         thumbnail: thumbnail)
    }
}
