//
//  PageMock.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import Foundation
@testable import MarvelChallenge

extension Page {

    static func mock(offset: Int = 0,
                     count: Int = 0,
                     totalCount: Int = 0) -> Page {
        return Page(offset: offset,
                    count: count,
                    totalCount: totalCount)
    }
}
