//
//  CharactersListMock.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import Foundation
@testable import MarvelChallenge

extension CharactersList {

    static func mock(characters: [Character] = [],
                     page: Page = Page.mock()) -> CharactersList {
        return CharactersList(characters: characters,
                              page: page)
    }
}
