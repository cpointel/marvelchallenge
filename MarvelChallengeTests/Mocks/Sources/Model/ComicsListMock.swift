//
//  ComicsListMock.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 27/10/2020.
//

import Foundation
@testable import MarvelChallenge

extension ComicsList {

    static func mock(comics: [Comic] = [],
                     page: Page = Page.mock()) -> ComicsList {
        return ComicsList(comics: comics,
                          page: page)
    }
}
