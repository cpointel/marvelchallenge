//
//  ThumbnailMock.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import Foundation
@testable import MarvelChallenge

extension Thumbnail {

    static func mock(path: String = "MOCK_PATH",
                     ext: String = "MOCK_EXT") -> Thumbnail {
        return Thumbnail(path: path,
                         ext: ext)
    }
}
