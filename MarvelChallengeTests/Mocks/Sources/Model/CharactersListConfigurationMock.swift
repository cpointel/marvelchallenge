//
//  CharactersListConfigurationMock.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import Foundation
@testable import MarvelChallenge

extension CharactersListConfiguration {

    static func mock(debounce: DispatchTimeInterval? = nil) -> CharactersListConfiguration {
        return CharactersListConfiguration(debounce: debounce)
    }
}
