//
//  ComicMock.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 27/10/2020.
//

import Foundation
@testable import MarvelChallenge

extension Comic {

    static func mock(id: Int = 0,
                     title: String = "MOCK_TITLE",
                     description: String = "MOCK_DESCRIPTION",
                     thumbnail: Thumbnail = Thumbnail.mock()) -> Comic {
        return Comic(id: id,
                     title: title,
                     description: description,
                     thumbnail: thumbnail)
    }
}
