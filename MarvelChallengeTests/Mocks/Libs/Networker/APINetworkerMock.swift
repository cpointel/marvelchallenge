//
//  APINetworkerMock.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 22/10/2020.
//

import Foundation
import RxSwift
@testable import MarvelChallenge

class APINetworkerMock: APINetworkerProtocol {

    // MARK: - MockProperties
    var mock_executeCalled: ((_ request: APINetworkerRequest) -> Void)?

    // MARK: - APINetworkerProtocol / Functions
    func execute<T>(request: APINetworkerRequest) -> Observable<T> where T: Decodable {
        mock_executeCalled?(request)
        return .never()
    }
}
