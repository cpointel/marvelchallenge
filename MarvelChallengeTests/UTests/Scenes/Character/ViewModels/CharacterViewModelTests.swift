//
//  CharacterViewModelTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 27/10/2020.
//

import XCTest
@testable import MarvelChallenge

class CharacterViewModelTests: RxXCTestCase {

    // MARK: - Utils
    private func createViewModel(character: Character = Character.mock(),
                                 services: CharacterViewModelServices = BusinessServicesMock(),
                                 routing: CharacterViewModelRouting? = nil) -> CharacterViewModel {
        return CharacterViewModel(character: character,
                                  services: services,
                                  routing: routing)
    }
}

// MARK: - var staticTexts: CharacterViewModelStaticTexts
extension CharacterViewModelTests {

    func test_staticTextsAreCorrect() {
        // Given
        let viewModel = createViewModel()

        // When
        let result = viewModel.staticTexts

        // Then
        XCTAssertEqual(result.descriptionSectionTitle, L10n.Scene.Character.Description.title)
        XCTAssertEqual(result.comicsSectionTitle, L10n.Scene.Character.Comics.title)
    }
}

// MARK: - var thumbnailURL: Driver<URL?>
extension CharacterViewModelTests {

    func test_thumbnailURL() {
        // Given
        let thumbnail = Thumbnail.mock(path: "http://www.image.fr",
                                       ext: "jpg")
        let character = Character.mock(thumbnail: thumbnail)
        let viewModel = createViewModel(character: character)

        // When
        let observer = scheduler.createObserver(URL?.self)
        viewModel.thumbnailURL
            .drive(observer)
            .disposed(by: disposeBag)
        scheduler.start()

        // Then
        XCTAssertEqual(observer.events, [
            .next(0, URL(string: "http://www.image.fr/landscape_amazing.jpg")),
            .completed(0)
        ])
    }
}

// MARK: - var title: Driver<String>
extension CharacterViewModelTests {

    func test_title() {
        // Given
        let character = Character.mock(name: "mock title")
        let viewModel = createViewModel(character: character)

        // When
        let observer = scheduler.createObserver(String.self)
        viewModel.title
            .drive(observer)
            .disposed(by: disposeBag)
        scheduler.start()

        // Then
        XCTAssertEqual(observer.events, [
            .next(0, "Mock Title"),
            .completed(0)
        ])
    }
}

// MARK: - var description: Driver<String>
extension CharacterViewModelTests {

    func test_description() {
        // Given
        let character = Character.mock(description: "MOCK_DESCRIPTION")
        let viewModel = createViewModel(character: character)

        // When
        let observer = scheduler.createObserver(String.self)
        viewModel.description
            .drive(observer)
            .disposed(by: disposeBag)
        scheduler.start()

        // Then
        XCTAssertEqual(observer.events, [
            .next(0, "MOCK_DESCRIPTION"),
            .completed(0)
        ])
    }
}

// MARK: - var comicsListViewModels: Driver<[ComicsListCellViewModelProtocol]>
extension CharacterViewModelTests {

    func test_comicsListViewModels_Should_Return2ViewModels_When_2Comics() {
        // Given
        let mockCharactersService = CharactersServiceMock()
        mockCharactersService.mock_comicsCalled = { _ in
            let comics = [
                Comic.mock(),
                Comic.mock()
            ]
            let mockPage = Page.mock()
            return .just(ComicsList(comics: comics, page: mockPage))
        }
        let mockBusinessServices = BusinessServicesMock()
        mockBusinessServices.charactersService = mockCharactersService

        let viewModel = createViewModel(services: mockBusinessServices)

        // When
        let observer = scheduler.createObserver(Int.self)
        viewModel.comicsListViewModels
            .map { $0.count }
            .drive(observer)
            .disposed(by: disposeBag)
        scheduler.start()

        // Then
        XCTAssertEqual(observer.events, [
            .next(0, 2)
        ])
    }

    func test_comicsListViewModels_Should_Return5ViewModels_When_5Comics() {
        // Given
        let mockCharactersService = CharactersServiceMock()
        mockCharactersService.mock_comicsCalled = { _ in
            let comics = [
                Comic.mock(),
                Comic.mock(),
                Comic.mock(),
                Comic.mock(),
                Comic.mock()
            ]
            let mockPage = Page.mock()
            return .just(ComicsList(comics: comics, page: mockPage))
        }
        let mockBusinessServices = BusinessServicesMock()
        mockBusinessServices.charactersService = mockCharactersService

        let viewModel = createViewModel(services: mockBusinessServices)

        // When
        let observer = scheduler.createObserver(Int.self)
        viewModel.comicsListViewModels
            .map { $0.count }
            .drive(observer)
            .disposed(by: disposeBag)
        scheduler.start()

        // Then
        XCTAssertEqual(observer.events, [
            .next(0, 5)
        ])
    }
}

// MARK: - var showDescription: Driver<Bool>
extension CharacterViewModelTests {

    func test_showDescription_Should_ReturnTrue_When_DescriptionExists() {
        // Given
        let character = Character.mock(description: "MOCK_DESCRIPTION")
        let viewModel = createViewModel(character: character)

        // When
        let observer = scheduler.createObserver(Bool.self)
        viewModel.showDescription
            .drive(observer)
            .disposed(by: disposeBag)
        scheduler.start()

        // Then
        XCTAssertEqual(observer.events, [
            .next(0, true),
            .completed(0)
        ])
    }

    func test_showDescription_Should_ReturnFalse_When_DescriptionIsEmpty() {
        // Given
        let character = Character.mock(description: "")
        let viewModel = createViewModel(character: character)

        // When
        let observer = scheduler.createObserver(Bool.self)
        viewModel.showDescription
            .drive(observer)
            .disposed(by: disposeBag)
        scheduler.start()

        // Then
        XCTAssertEqual(observer.events, [
            .next(0, false),
            .completed(0)
        ])
    }
}

// MARK: - var showComics: Driver<Bool>
extension CharacterViewModelTests {

    func test_showComics_Should_ReturnTrue_When_ComicsIsNotEmpty() {
        // Given
        let mockCharactersService = CharactersServiceMock()
        mockCharactersService.mock_comicsCalled = { _ in
            let mockComic = Comic.mock()
            let mockPage = Page.mock()
            return .just(ComicsList(comics: [mockComic], page: mockPage))
        }
        let mockBusinessServices = BusinessServicesMock()
        mockBusinessServices.charactersService = mockCharactersService

        let viewModel = createViewModel(services: mockBusinessServices)

        // When
        let observer = scheduler.createObserver(Bool.self)
        viewModel.showComics
            .drive(observer)
            .disposed(by: disposeBag)
        scheduler.start()

        // Then
        XCTAssertEqual(observer.events, [
            .next(0, true)
        ])
    }

    func test_showComics_Should_ReturnFalse_When_ComicsIsEmpty() {
        // Given
        let mockCharactersService = CharactersServiceMock()
        mockCharactersService.mock_comicsCalled = { _ in
            let mockPage = Page.mock()
            return .just(ComicsList(comics: [], page: mockPage))
        }
        let mockBusinessServices = BusinessServicesMock()
        mockBusinessServices.charactersService = mockCharactersService

        let viewModel = createViewModel(services: mockBusinessServices)

        // When
        let observer = scheduler.createObserver(Bool.self)
        viewModel.showComics
            .drive(observer)
            .disposed(by: disposeBag)
        scheduler.start()

        // Then
        XCTAssertEqual(observer.events, [
            .next(0, false)
        ])
    }
}

// MARK: - func didTapOnBackButton()
extension CharacterViewModelTests {

    func test_didTapOnBackButton() {
        // Given
        let character = Character.mock()
        let mockRouting = CharacterRoutingMock()
        let mockRoutingExpectation = self.expectation(description: "CharacterRouting.back() should be called")
        mockRouting.mock_backCalled = {
            mockRoutingExpectation.fulfill()
        }

        let viewModel = createViewModel(character: character,
                                        routing: mockRouting)

        // When
        viewModel.didTapOnBackButton()

        // Then
        waitForExpectations(timeout: 0.1)
    }
}
