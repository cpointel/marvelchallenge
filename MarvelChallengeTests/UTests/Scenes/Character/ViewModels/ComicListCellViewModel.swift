//
//  ComicListCellViewModelTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 27/10/2020.
//

import XCTest
@testable import MarvelChallenge

class ComicListCellViewModelTests: XCTestCase {

    // MARK: - Utils
    private func createViewModel(comic: Comic = Comic.mock()) -> ComicsListCellViewModel {
        return ComicsListCellViewModel(comic: comic)
    }
}

// MARK: - var title: String
extension ComicListCellViewModelTests {

    func test_title() {
        // Given
        let comic = Comic.mock(title: "mock name")

        // When
        let viewModel = createViewModel(comic: comic)

        // Then
        XCTAssertEqual(viewModel.title, "mock name")
    }
}

// MARK: - var description: String
extension ComicListCellViewModelTests {

    func test_description() {
        // Given
        let comic = Comic.mock(description: "MOCK_DESCRIPTION")

        // When
        let viewModel = createViewModel(comic: comic)

        // Then
        XCTAssertEqual(viewModel.description, "MOCK_DESCRIPTION")
    }
}

// MARK: - var thumbnailURL: URL?
extension ComicListCellViewModelTests {

    func test_thumbnailURL() {
        // Given
        let thumbnail = Thumbnail.mock(path: "http://www.image.fr",
                                       ext: "jpg")
        let comic = Comic.mock(thumbnail: thumbnail)

        // When
        let viewModel = createViewModel(comic: comic)

        // Then
        XCTAssertEqual(viewModel.thumbnailURL, URL(string: "http://www.image.fr/portrait_uncanny.jpg"))
    }
}
