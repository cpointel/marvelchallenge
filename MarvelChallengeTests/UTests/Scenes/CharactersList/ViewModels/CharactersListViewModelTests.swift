//
//  CharactersListViewModelTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import XCTest
@testable import MarvelChallenge

class CharactersListViewModelTests: RxXCTestCase {

    // MARK: - Utils
    private func createViewModel(configuration: CharactersListConfiguration = CharactersListConfiguration.mock(),
                                 services: CharactersListViewModelServices = BusinessServicesMock(),
                                 routing: CharactersListViewModelRouting = CharactersListRoutingMock()) -> CharactersListViewModel {
        return CharactersListViewModel(configuration: configuration,
                                       services: services,
                                       routing: routing)
    }
}

// MARK: - var staticTexts: CharactersListStaticTexts
extension CharactersListViewModelTests {

    func test_staticTextsAreCorrect() {
        // Given
        let viewModel = createViewModel()

        // When
        let result = viewModel.staticTexts

        // Then
        XCTAssertEqual(result.searchPlaceholder, L10n.Scene.Characters.SearchBar.placeholder)
    }
}

// MARK: - var viewType: Driver<CharactersListViewType>
extension CharactersListViewModelTests {

    func test_viewType() {
        // Given
        let charactersServiceMock = CharactersServiceMock()
        let businessServicesMock = BusinessServicesMock()
        businessServicesMock.charactersService = charactersServiceMock

        charactersServiceMock.mock_charactersCalled = { [weak self] _, _, _ in
            guard let clock = self?.scheduler.clock else {
                XCTFail("Should not be called")
                fatalError()
            }
            switch clock {
            case 0, 2, 3, 5: return .just(CharactersList.mock())
            case 1, 4, 6, 7: return .just(CharactersList.mock(characters: [Character.mock()]))
            default:
                XCTFail("Should not be called")
                fatalError()
            }
        }

        let viewModel = createViewModel(services: businessServicesMock)
        scheduler.scheduleAt(1) { viewModel.didTypeKeywords("a") }
        scheduler.scheduleAt(2) { viewModel.didTypeKeywords("ab") }
        scheduler.scheduleAt(3) { viewModel.didTypeKeywords("abc") }
        scheduler.scheduleAt(4) { viewModel.didTypeKeywords("abcd") }
        scheduler.scheduleAt(5) { viewModel.didTypeKeywords("abc") }
        scheduler.scheduleAt(6) { viewModel.didTypeKeywords("abcd") }
        scheduler.scheduleAt(7) { viewModel.didTypeKeywords(nil) }

        // When
        let observer = scheduler.createObserver(CharactersListViewType.self)
        viewModel.viewType
            .drive(observer)
            .disposed(by: disposeBag)
        scheduler.start()

        // Then
        XCTAssertEqual(observer.events, [
            .next(0, .noResult),
            .next(1, .characters(viewModels: [], isMoreResults: false)),
            .next(2, .noResult),
            .next(3, .noResult),
            .next(4, .characters(viewModels: [], isMoreResults: false)),
            .next(5, .noResult),
            .next(6, .characters(viewModels: [], isMoreResults: false)),
            .next(7, .characters(viewModels: [], isMoreResults: false))
        ])
    }
}

// MARK: - var isLoading: Driver<Bool>
extension CharactersListViewModelTests {

    func test_isLoading() {
        // TODO: UnitTests
    }
}

// MARK: - var isLoadingMore: Driver<Bool>
extension CharactersListViewModelTests {

    func test_isLoadingMore() {
        // TODO: UnitTests
    }
}

// MARK: - func viewDidLoad()
extension CharactersListViewModelTests {

    func test_viewDidLoad() {
        // TODO: UnitTests
    }
}

// MARK: - func didTypeKeywords(_ keywords: String?)
extension CharactersListViewModelTests {

    func test_didTypeKeywords() {
        // TODO: UnitTests
    }
}

// MARK: - func didCancelSearch()
extension CharactersListViewModelTests {

    func test_didCancelSearch() {
        // TODO: UnitTests
    }
}

// MARK: - func didScrollToItem(at index: Int)
extension CharactersListViewModelTests {

    func test_didScrollToItem() {
        // TODO: UnitTests
    }
}
