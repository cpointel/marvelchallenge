//
//  CharactersListNoResultViewModelTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import XCTest
@testable import MarvelChallenge

class CharactersListNoResultViewModelTests: XCTestCase {}

extension CharactersListNoResultViewModelTests {

    func test_init() {
        // Given
        // When
        let viewModel = CharactersListNoResultViewModel()

        // Then
        XCTAssertEqual(viewModel.title, L10n.Scene.Characters.Placeholder.NoResult.title)
        XCTAssertEqual(viewModel.description, L10n.Scene.Characters.Placeholder.NoResult.description)
        XCTAssertEqual(viewModel.image, Asset.Illustrations.illNoResult.image)
        XCTAssertEqual(viewModel.imageSizeType, .ratio(1))
        XCTAssertEqual(viewModel.actionTitle, nil)
        XCTAssertNil(viewModel.action)
    }
}
