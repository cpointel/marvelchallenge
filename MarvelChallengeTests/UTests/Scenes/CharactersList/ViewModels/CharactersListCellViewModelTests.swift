//
//  CharactersListCellViewModelTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import XCTest
@testable import MarvelChallenge

class CharactersListCellViewModelTests: XCTestCase {

    // MARK: - Utils
    private func createViewModel(character: Character = Character.mock(),
                                 routing: CharactersListCellViewModelRouting? = nil) -> CharactersListCellViewModel {
        return CharactersListCellViewModel(with: character, routing: routing)
    }
}

// MARK: - var name: String
extension CharactersListCellViewModelTests {

    func test_name() {
        // Given
        let character = Character.mock(name: "mock name")

        // When
        let viewModel = createViewModel(character: character)

        // Then
        XCTAssertEqual(viewModel.name, "Mock Name")
    }
}

// MARK: - var thumbnailURL: URL?
extension CharactersListCellViewModelTests {

    func test_thumbnailURL() {
        // Given
        let thumbnail = Thumbnail.mock(path: "http://www.image.fr",
                                       ext: "jpg")
        let character = Character.mock(thumbnail: thumbnail)

        // When
        let viewModel = createViewModel(character: character)

        // Then
        XCTAssertEqual(viewModel.thumbnailURL, URL(string: "http://www.image.fr/standard_xlarge.jpg"))
    }
}

// MARK: func didTapOnView()
extension CharactersListCellViewModelTests {

    func test_didTapOnView() {
        // Given
        let character = Character.mock()
        let mockRouting = CharactersListRoutingMock()
        let mockRoutingExpectation = self.expectation(description: "CharactersListRouting.showCharacter should be called")
        var resultCharacter: Character?
        mockRouting.mock_showCharacterCalled = { character in
            resultCharacter = character
            mockRoutingExpectation.fulfill()
        }

        let viewModel = createViewModel(character: character,
                                        routing: mockRouting)

        // When
        viewModel.didTapOnView()

        // Then
        waitForExpectations(timeout: 0.1)
        XCTAssertEqual(resultCharacter, character)
    }
}
