//
//  CharacterTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import XCTest
@testable import MarvelChallenge

class CharacterTests: XCTestCase {}

// MARK: - init
extension CharacterTests {

    func test_init_when_thumbnail() {
        // Given
        // When
        let result = Character(id: 10,
                               name: "MOCK_NAME",
                               description: "MOCK_DESCRIPTION",
                               thumbnail: Thumbnail.mock())

        // Then
        XCTAssertEqual(result.id, 10)
        XCTAssertEqual(result.name, "MOCK_NAME")
        XCTAssertEqual(result.description, "MOCK_DESCRIPTION")
        XCTAssertNotNil(result.thumbnail)
    }

    func test_init_when_noThumbnail() {
        // Given
        // When
        let result = Character(id: 10,
                               name: "MOCK_NAME",
                               description: "MOCK_DESCRIPTION",
                               thumbnail: nil)

        // Then
        XCTAssertEqual(result.id, 10)
        XCTAssertEqual(result.name, "MOCK_NAME")
        XCTAssertEqual(result.description, "MOCK_DESCRIPTION")
        XCTAssertNil(result.thumbnail)
    }
}
