//
//  PageTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import XCTest
@testable import MarvelChallenge

class PageTests: XCTestCase {}

// MARK: - init
extension PageTests {

    func test_init() {
        // Given
        // When
        let result = Page(offset: 10,
                          count: 15,
                          totalCount: 100)

        // Then
        XCTAssertEqual(result.offset, 10)
        XCTAssertEqual(result.count, 15)
        XCTAssertEqual(result.totalCount, 100)
    }
}
