//
//  ComicTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import XCTest
@testable import MarvelChallenge

class ComicTests: XCTestCase {}

// MARK: - init
extension ComicTests {

    func test_init_when_noOptionalValues() {
        // Given
        // When
        let result = Comic(id: 10,
                           title: nil,
                           description: nil,
                           thumbnail: nil)
        // Then
        XCTAssertEqual(result.id, 10)
        XCTAssertNil(result.title)
        XCTAssertNil(result.description)
        XCTAssertNil(result.thumbnail)
    }

    func test_init_when_allOptionalValues() {
        // Given
        // When
        let result = Comic(id: 10,
                           title: "MOCK_TITLE",
                           description: "MOCK_DESCRIPTION",
                           thumbnail: Thumbnail.mock())

        // Then
        XCTAssertEqual(result.id, 10)
        XCTAssertEqual(result.title, "MOCK_TITLE")
        XCTAssertEqual(result.description, "MOCK_DESCRIPTION")
        XCTAssertNotNil(result.thumbnail)
    }
}
