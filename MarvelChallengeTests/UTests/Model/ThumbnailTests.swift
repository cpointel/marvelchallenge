//
//  ThumbnailTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import XCTest
@testable import MarvelChallenge

class ThumbnailTests: XCTestCase {}

// MARK: - init
extension ThumbnailTests {

    func test_init() {
        // Given
        // When
        let result = Thumbnail(path: "MOCK_PATH",
                               ext: "MOCK_EXTENSION")

        // Then
        XCTAssertEqual(result.path, "MOCK_PATH")
        XCTAssertEqual(result.ext, "MOCK_EXTENSION")
    }
}
