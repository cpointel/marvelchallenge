//
//  ImagesServiceTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import XCTest
@testable import MarvelChallenge

class ImagesServiceTests: XCTestCase {}

// MARK: - static func imageURL(path: String, ext: String, type: ImageType) -> URL?
extension ImagesServiceTests {

    func genericTest_imageURL(path: String,
                              ext: String,
                              type: ImageType,
                              expectedResult: String,
                              file: StaticString = #file,
                              line: UInt = #line) {
        // Given
        // When
        let result = ImagesService.imageURL(path: path,
                                            ext: ext,
                                            type: type)

        // Then
        XCTAssertEqual(result?.absoluteString, expectedResult, file: file, line: line)
    }

    func test_imageURL() {
        genericTest_imageURL(path: "MOCK_PATH1",
                             ext: "MOCK_EXT1",
                             type: .standardMedium,
                             expectedResult: "MOCK_PATH1/standard_medium.MOCK_EXT1")
        genericTest_imageURL(path: "MOCK_PATH2",
                             ext: "MOCK_EXT2",
                             type: .standardXLarge,
                             expectedResult: "MOCK_PATH2/standard_xlarge.MOCK_EXT2")
        genericTest_imageURL(path: "MOCK_PATH3",
                             ext: "MOCK_EXT3",
                             type: .portraitUncanny,
                             expectedResult: "MOCK_PATH3/portrait_uncanny.MOCK_EXT3")
        genericTest_imageURL(path: "MOCK_PATH4",
                             ext: "MOCK_EXT4",
                             type: .landscapeAmazing,
                             expectedResult: "MOCK_PATH4/landscape_amazing.MOCK_EXT4")
    }
}
