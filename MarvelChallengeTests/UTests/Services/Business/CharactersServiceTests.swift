//
//  CharactersServiceTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 24/10/2020.
//

import XCTest
@testable import MarvelChallenge

class CharactersServiceTests: RxXCTestCase {

    // MARK: - Utils
    private func createService(dataAccess: MarvelApiProtocol = MarvelApiMock()) -> CharactersService {
        return CharactersService(dataAccess: dataAccess)
    }
}

// MARK: - func characters(nameStartsWith: String?, atOffset offset: Int, limit: Int) -> Single<CharactersList>
extension CharactersServiceTests {

    func test_characters() {
        // Given
        let mockNameStartsWith: String? = "MOCK_NAME_STARTS_WITH"
        let mockOffset: Int = 10
        let mockLimit: Int = 20

        let mockMarvelApi = MarvelApiMock()

        let mockMarvelApiExpectation = self.expectation(description: "MarvelApi.fetchCharacters should be called")
        var resultRequest: APICharactersRequestDTO?
        mockMarvelApi.mock_fetchCharactersCalled = { request in
            resultRequest = request
            mockMarvelApiExpectation.fulfill()

            let character = APICharacterDTO.mock()
            let container: APIDataContainerDTO<APICharacterDTO>
            container = APIDataContainerDTO.mock(results: [character])
            let wrapper: APIDataWrapperDTO<APICharacterDTO>
            wrapper = APIDataWrapperDTO.mock(data: container)
            return .just(wrapper)
        }

        let service = createService(dataAccess: mockMarvelApi)

        // When
        let observer = scheduler.createObserver(CharactersList.self)
        service.characters(nameStartsWith: mockNameStartsWith,
                           atOffset: mockOffset,
                           limit: mockLimit)
            .asObservable()
            .subscribe(observer)
            .disposed(by: disposeBag)
        scheduler.start()

        // Then
        waitForExpectations(timeout: 0.1)
        XCTAssertEqual(resultRequest?.nameStartsWith, "MOCK_NAME_STARTS_WITH")
        XCTAssertEqual(resultRequest?.offset, 10)
        XCTAssertEqual(resultRequest?.limit, 20)

        let expectedThumbnail = Thumbnail(path: "MOCK_PATH",
                                          ext: "MOCK_EXT")
        let expectedCharacter = Character(id: 100,
                                          name: "MOCK_NAME",
                                          description: "MOCK_DESCRIPTION",
                                          thumbnail: expectedThumbnail)
        let expectedPage = Page(offset: 10,
                                count: 7,
                                totalCount: 100)
        let expectedCharactersList = CharactersList(characters: [expectedCharacter],
                                                    page: expectedPage)
        XCTAssertEqual(observer.events, [
            .next(0, expectedCharactersList),
            .completed(0)
        ])
    }
}

// MARK: - func comics(for characterId: Int) -> Single<ComicsList>
extension CharactersServiceTests {

    func test_comics() {
        // Given
        let mockCharacterId: Int = 1747282

        let mockMarvelApi = MarvelApiMock()

        let mockMarvelApiExpectation = self.expectation(description: "MarvelApi.fetchComics should be called")
        var resultCharacterId: Int?
        var resultRequest: APIComicsRequestDTO?
        mockMarvelApi.mock_fetchComicsCalled = { characterId, request in
            resultCharacterId = characterId
            resultRequest = request
            mockMarvelApiExpectation.fulfill()

            let comic = APIComicDTO.mock()
            let container: APIDataContainerDTO<APIComicDTO>
            container = APIDataContainerDTO.mock(results: [comic])
            let wrapper: APIDataWrapperDTO<APIComicDTO>
            wrapper = APIDataWrapperDTO.mock(data: container)
            return .just(wrapper)
        }

        let service = createService(dataAccess: mockMarvelApi)

        // When
        let observer = scheduler.createObserver(ComicsList.self)
        service.comics(for: mockCharacterId)
            .asObservable()
            .subscribe(observer)
            .disposed(by: disposeBag)
        scheduler.start()

        // Then
        waitForExpectations(timeout: 0.1)
        XCTAssertEqual(resultCharacterId, 1747282)
        XCTAssertEqual(resultRequest?.offset, 0)
        XCTAssertEqual(resultRequest?.limit, 20)

        let expectedThumbnail = Thumbnail(path: "MOCK_PATH",
                                          ext: "MOCK_EXT")
        let expectedComic = Comic(id: 100,
                                  title: "MOCK_TITLE",
                                  description: "MOCK_DESCRIPTION",
                                  thumbnail: expectedThumbnail)
        let expectedPage = Page(offset: 10,
                                count: 7,
                                totalCount: 100)
        let expectedComicsList = ComicsList(comics: [expectedComic],
                                            page: expectedPage)
        XCTAssertEqual(observer.events, [
            .next(0, expectedComicsList),
            .completed(0)
        ])
    }
}
