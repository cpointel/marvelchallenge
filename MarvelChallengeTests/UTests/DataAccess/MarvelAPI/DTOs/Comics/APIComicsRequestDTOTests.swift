//
//  APIComicsRequestDTOTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 27/10/2020.
//

import XCTest
@testable import MarvelChallenge

class APIComicsRequestDTOTests: XCTestCase {}

// MARK: - Init
extension APIComicsRequestDTOTests {

    func genericTest_init(offset: Int,
                          limit: Int,
                          file: StaticString = #file,
                          line: UInt = #line) {
        // Given
        // When
        let result = APIComicsRequestDTO(atOffset: offset,
                                         limit: limit)

        // Then
        XCTAssertEqual(result.offset, offset, file: file, line: line)
        XCTAssertEqual(result.limit, limit, file: file, line: line)
    }

    func test_init() {
        genericTest_init(offset: 0, limit: 10)
        genericTest_init(offset: 10, limit: 25)
        genericTest_init(offset: 5, limit: 5)
    }
}
