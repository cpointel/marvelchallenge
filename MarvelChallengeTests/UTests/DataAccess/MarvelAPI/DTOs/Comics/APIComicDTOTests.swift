//
//  APIComicDTOTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 27/10/2020.
//

import XCTest
@testable import MarvelChallenge

class APIComicDTOTests: XCTestCase {}

// MARK: - Init - Decodable
extension APIComicDTOTests {

    func test_init() {
        // Given
        let file = "Fixtures/comic"

        // When
        let result: APIComicDTO? = getObject(fromFile: file)

        // Then
        XCTAssertNotNil(result)
        if let result = result {
            XCTAssertEqual(result.id, 1308)
            XCTAssertEqual(result.title, "Marvel Age Spider-Man Vol. 2: Everyday Hero (Digest)")
            // swiftlint:disable:next line_length
            XCTAssertEqual(result.description, "The Marvel Age of Comics continues! This time around, Spidey meets his match against such classic villains as Electro and the Lizard, and faces the return of one of his first foes: the Vulture! Plus: Spider-Man vs. the Living Brain, Peter Parker's fight with Flash Thompson, and the wall-crawler tackles the high-flying Human Torch!")
            XCTAssertNotNil(result.thumbnail)
        }
    }
}
