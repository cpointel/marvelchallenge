//
//  APICharactersOrderByTypeDTOTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 22/10/2020.
//

import XCTest
@testable import MarvelChallenge

class APICharactersOrderByTypeDTOTests: XCTestCase {}

// MARK: - Init - Decodable
extension APICharactersOrderByTypeDTOTests {

    func genericTest_init(type: APICharactersOrderByDTO,
                          expectedRawValue: String,
                          file: StaticString = #file,
                          line: UInt = #line) {
        // Then
        XCTAssertEqual(type.rawValue, expectedRawValue, file: file, line: line)
    }

    func test_init() {
        genericTest_init(type: .ascName, expectedRawValue: "name")
        genericTest_init(type: .dscName, expectedRawValue: "-name")
        genericTest_init(type: .ascModified, expectedRawValue: "modified")
        genericTest_init(type: .dscModified, expectedRawValue: "-modified")
    }
}
