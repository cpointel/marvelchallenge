//
//  APICharactersRequestDTOTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 22/10/2020.
//

import XCTest
@testable import MarvelChallenge

class APICharactersRequestDTOTests: XCTestCase {}

// MARK: - Init
extension APICharactersRequestDTOTests {

    func genericTest_init(nameStartsWith: String?,
                          offset: Int,
                          limit: Int,
                          orderBy: APICharactersOrderByDTO,
                          file: StaticString = #file,
                          line: UInt = #line) {
        // Given
        // When
        let result = APICharactersRequestDTO(nameStartsWith: nameStartsWith,
                                             atOffset: offset,
                                             limit: limit,
                                             orderBy: orderBy)

        // Then
        XCTAssertEqual(result.nameStartsWith, nameStartsWith, file: file, line: line)
        XCTAssertEqual(result.offset, offset, file: file, line: line)
        XCTAssertEqual(result.limit, limit, file: file, line: line)
        XCTAssertEqual(result.orderBy, orderBy, file: file, line: line)
    }

    func test_init() {
        genericTest_init(nameStartsWith: nil, offset: 0, limit: 10, orderBy: .ascModified)
        genericTest_init(nameStartsWith: "MOCK_NAME_STARTS_WITH_1", offset: 0, limit: 10, orderBy: .ascModified)
        genericTest_init(nameStartsWith: "MOCK_NAME_STARTS_WITH_2", offset: 10, limit: 25, orderBy: .dscModified)
        genericTest_init(nameStartsWith: "MOCK_NAME_STARTS_WITH_3", offset: 5, limit: 5, orderBy: .ascName)
        genericTest_init(nameStartsWith: "MOCK_NAME_STARTS_WITH_4", offset: 5, limit: 10, orderBy: .dscName)
    }
}
