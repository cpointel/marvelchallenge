//
//  APIThumbnailDTOTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 22/10/2020.
//

import XCTest
@testable import MarvelChallenge

class APIThumbnailDTOTests: XCTestCase {}

// MARK: - Init - Decodable
extension APIThumbnailDTOTests {

    func test_init() {
        // Given
        let file = "Fixtures/thumbnail"

        // When
        let result: APIThumbnailDTO? = getObject(fromFile: file)

        // Then
        XCTAssertNotNil(result)
        if let result = result {
            XCTAssertEqual(result.path, "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available")
            XCTAssertEqual(result.ext, "jpg")
        }
    }
}
