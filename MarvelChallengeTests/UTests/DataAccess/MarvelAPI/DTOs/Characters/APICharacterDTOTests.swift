//
//  APICharacterDTOTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 22/10/2020.
//

import XCTest
@testable import MarvelChallenge

class APICharacterDTOTests: XCTestCase {}

// MARK: - Init - Decodable
extension APICharacterDTOTests {

    func test_init() {
        // Given
        let file = "Fixtures/character"

        // When
        let result: APICharacterDTO? = getObject(fromFile: file)

        // Then
        XCTAssertNotNil(result)
        if let result = result {
            XCTAssertEqual(result.id, 1009262)
            XCTAssertEqual(result.name, "Daredevil")
            // swiftlint:disable:next line_length
            XCTAssertEqual(result.description, "Abandoned by his mother, Matt Murdock was raised by his father, boxer \"Battling Jack\" Murdock, in Hell's Kitchen. Realizing that rules were needed to prevent people from behaving badly, young Matt decided to study law; however, when he saved a man from an oncoming truck, it spilled a radioactive cargo that rendered Matt blind while enhancing his remaining senses. Under the harsh tutelage of blind martial arts master Stick, Matt mastered his heightened senses and became a formidable fighter.")
            XCTAssertNotNil(result.thumbnail)
        }
    }
}
