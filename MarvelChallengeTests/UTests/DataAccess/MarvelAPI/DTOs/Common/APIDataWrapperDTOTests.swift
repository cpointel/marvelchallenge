//
//  APIDataWrapperDTOTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 22/10/2020.
//

import XCTest
@testable import MarvelChallenge

class APIDataWrapperDTOTests: XCTestCase {}

// MARK: - Init - Decodable
extension APIDataWrapperDTOTests {

    func test_init() {
        // Given
        let file = "Fixtures/dataWrapper"

        // When
        let result: APIDataWrapperDTO<String>? = getObject(fromFile: file)

        // Then
        XCTAssertNotNil(result)
        if let result = result {
            XCTAssertEqual(result.code, 200)
            XCTAssertNotNil(result.data)
        }
    }
}
