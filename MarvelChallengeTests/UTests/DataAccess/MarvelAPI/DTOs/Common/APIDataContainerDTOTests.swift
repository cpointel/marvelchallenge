//
//  APIDataContainerDTOTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 22/10/2020.
//

import XCTest
@testable import MarvelChallenge

class APIDataContainerDTOTests: XCTestCase {}

// MARK: - Init - Decodable
extension APIDataContainerDTOTests {

    func test_init() {
        // Given
        let file = "Fixtures/dataContainer"

        // When
        let result: APIDataContainerDTO<String>? = getObject(fromFile: file)

        // Then
        XCTAssertNotNil(result)
        if let result = result {
            XCTAssertEqual(result.offset, 15)
            XCTAssertEqual(result.limit, 10)
            XCTAssertEqual(result.total, 20)
            XCTAssertEqual(result.count, 5)
            XCTAssertEqual(result.results, ["resultA", "resultB"])
        }
    }
}
