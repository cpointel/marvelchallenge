//
//  MarvelApiTests.swift
//  MarvelChallengeTests
//
//  Created by Cedric Pointel on 22/10/2020.
//

import XCTest
@testable import MarvelChallenge

class MarvelApiTests: XCTestCase {

    // MARK: - Utils
    private func createMarvelApi(networker: APINetworkerProtocol = APINetworkerMock(),
                                 apiKey: String = "MOCK_API_KEY",
                                 timestamp: String = "MOCK_TIME_STAMP",
                                 hash: String = "MOCK_HASH") -> MarvelApi {
        return MarvelApi(networker: networker,
                         apiKey: apiKey,
                         timestamp: timestamp,
                         hash: hash)
    }
}

// MARK: - func fetchCharacters(dto: APICharactersRequestDTO) -> Single<[APICharacterDTO]>
extension MarvelApiTests {

    func test_fetchCharacters_checkRequest() throws {

        // Given
        let dto = APICharactersRequestDTO(nameStartsWith: "MOCK_NAME_STARTS_WITH",
                                          atOffset: 34,
                                          limit: 24,
                                          orderBy: .ascName)
        let mockNetworker = APINetworkerMock()
        var resultRequest: APINetworkerRequest?
        mockNetworker.mock_executeCalled = { request in
            resultRequest = request
        }
        let api = createMarvelApi(networker: mockNetworker)

        // When
        _ = api.fetchCharacters(dto: dto)

        // Then
        XCTAssertNotNil(resultRequest)
        XCTAssertEqual(resultRequest?.method, .get)
        XCTAssertEqual(resultRequest?.headers, [:])
        XCTAssertEqual(resultRequest?.encoding, .urlEncoded)
        XCTAssertEqual(resultRequest?.path, "/v1/public/characters")
        let parameters = try XCTUnwrap(resultRequest?.parameters)
        XCTAssertEqual(parameters.count, 7)
        XCTAssertEqual(parameters["apikey"] as? String, "MOCK_API_KEY")
        XCTAssertEqual(parameters["ts"] as? String, "MOCK_TIME_STAMP")
        XCTAssertEqual(parameters["hash"] as? String, "MOCK_HASH")
        XCTAssertEqual(parameters["nameStartsWith"] as? String, "MOCK_NAME_STARTS_WITH")
        XCTAssertEqual(parameters["offset"] as? Int, 34)
        XCTAssertEqual(parameters["limit"] as? Int, 24)
        XCTAssertEqual(parameters["orderBy"] as? String, "name")
    }
}

// MARK: - func fetchComics(for characterId: Int, dto: APIComicsRequestDTO) -> Single<APIDataWrapperDTO<APIComicDTO>>
extension MarvelApiTests {

    func test_fetchComics_checkRequest() throws {

        // Given
        let characterId: Int = 1023432
        let dto = APIComicsRequestDTO(atOffset: 34,
                                      limit: 24)
        let mockNetworker = APINetworkerMock()
        var resultRequest: APINetworkerRequest?
        mockNetworker.mock_executeCalled = { request in
            resultRequest = request
        }
        let api = createMarvelApi(networker: mockNetworker)

        // When
        _ = api.fetchComics(for: characterId, dto: dto)

        // Then
        XCTAssertNotNil(resultRequest)
        XCTAssertEqual(resultRequest?.method, .get)
        XCTAssertEqual(resultRequest?.headers, [:])
        XCTAssertEqual(resultRequest?.encoding, .urlEncoded)
        XCTAssertEqual(resultRequest?.path, "/v1/public/characters/1023432/comics")
        let parameters = try XCTUnwrap(resultRequest?.parameters)
        XCTAssertEqual(parameters.count, 5)
        XCTAssertEqual(parameters["apikey"] as? String, "MOCK_API_KEY")
        XCTAssertEqual(parameters["ts"] as? String, "MOCK_TIME_STAMP")
        XCTAssertEqual(parameters["hash"] as? String, "MOCK_HASH")
        XCTAssertEqual(parameters["offset"] as? Int, 34)
        XCTAssertEqual(parameters["limit"] as? Int, 24)
    }
}
