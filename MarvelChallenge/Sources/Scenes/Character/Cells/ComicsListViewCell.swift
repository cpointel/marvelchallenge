//
//  ComicsListViewCell.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 26/10/2020.
//

import Reusable
import AlamofireImage

final internal class ComicsListViewCell: UICollectionViewCell, NibReusable {

    // MARK: - IBOutlets
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var imageViewActivityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!

    // MARK: - Birth and Death
    override func awakeFromNib() {
        super.awakeFromNib()
        setupStyle()
    }

    // MARK: - Setup
    private func setupStyle() {
        [contentView, imageView, descriptionLabel, titleLabel].apply(style: Style().backgroundColor(.app(.background)))

        self.imageViewActivityIndicator.apply(style: Style().color(.raw(.white)))
        self.titleLabel.apply(style: Style()
                                .font(.medium(.large))
                                .textColor(.app(.text))
                                .align(.left))
        self.descriptionLabel.apply(style: Style()
                                        .font(.regular(.small))
                                        .textColor(.app(.text))
                                        .align(.left))
    }

    func setup(viewModel: ComicsListCellViewModelProtocol) {
        self.titleLabel.text = viewModel.title
        self.descriptionLabel.text = viewModel.description
        if let thumbnailURL = viewModel.thumbnailURL {
            self.imageView.image = nil

            self.imageViewActivityIndicator.startAnimating()
            self.imageView.af_setImage(withURL: thumbnailURL, imageTransition: .crossDissolve(0.33)) { [weak self] _ in
                self?.imageViewActivityIndicator.stopAnimating()
            }
        } else {
            self.imageView.image = nil
        }
    }
}
