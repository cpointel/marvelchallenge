//
//  CharacterViewController.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 25/10/2020.
//

import Reusable
import RxSwift
import RxCocoa

internal final class CharacterViewController: UIViewController, StoryboardBased {

    // MARK: - IBOutlets
    @IBOutlet private weak var topBar: UINavigationBar!
    @IBOutlet private weak var scrollView: UIScrollView!

    @IBOutlet private weak var thumbnailImageView: UIImageView!

    @IBOutlet private weak var descriptionView: UIView!
    @IBOutlet private weak var descriptionTitleLabel: UILabel!
    @IBOutlet private weak var descriptionValueLabel: UILabel!

    @IBOutlet private weak var comicsView: UIView!
    @IBOutlet private weak var comicsTitleLabel: UILabel!
    @IBOutlet private weak var comicsCollectionView: UICollectionView!

    // MARK: - Properties
    private var viewModel: CharacterViewModelProtocol!
    private var cellViewModels: [ComicsListCellViewModelProtocol] = []
    private let disposeBag = DisposeBag()

    // MARK: - Birth and Death
    static func instantiate(viewModel: CharacterViewModelProtocol) -> CharacterViewController {
        let viewController = CharacterViewController.instantiate()
        viewController.viewModel = viewModel
        return viewController
    }

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupStaticTexts()
        setupAccessibilityIdentifiers()
        setupCollectionView()
        setupStyles()
        setupBindings()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if let layout = self.comicsCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let width = (comicsCollectionView.innerWidth() - layout.minimumInteritemSpacing)
            let size = CGSize(width: width, height: width * 30 / 45)
            layout.itemSize = size
        }
        self.comicsCollectionView.collectionViewLayout.invalidateLayout()
    }

    // MARK: - Setup
    private func setupStaticTexts() {
        self.descriptionTitleLabel.text = viewModel.staticTexts.descriptionSectionTitle
        self.comicsTitleLabel.text = viewModel.staticTexts.comicsSectionTitle
    }

    private func setupAccessibilityIdentifiers() {
        self.view.setAccessibility(.sceneCharacterScreenPage)
        self.descriptionView.setAccessibility(.sceneCharacterDescriptionView)
        self.comicsView.setAccessibility(.sceneCharacterComicsView)
    }

    private func setupCollectionView() {
        self.comicsCollectionView.register(cellType: ComicsListViewCell.self)
        self.comicsCollectionView.dataSource = self
    }

    private func setupBindings() {
        if let topItem = topBar.topItem {
            self.viewModel.title.drive(topItem.rx.title).disposed(by: disposeBag)
        }

        self.viewModel.thumbnailURL
            .drive { [weak self] imageURL in
                self?.thumbnailImageView.image = nil
                if let imageURL = imageURL {
                    self?.thumbnailImageView.af_setImage(withURL: imageURL, imageTransition: .crossDissolve(0.33))
                }
            }
            .disposed(by: disposeBag)

        self.viewModel.description.drive(descriptionValueLabel.rx.text).disposed(by: disposeBag)
        self.viewModel.showDescription.map(!).drive(descriptionView.rx.isHidden).disposed(by: disposeBag)
        self.viewModel.comicsListViewModels
            .drive { [weak self] viewModels in
                self?.updateComicsViewModels(viewModels)
            }
            .disposed(by: disposeBag)
        self.viewModel.showComics.map(!).drive(comicsView.rx.isHidden).disposed(by: disposeBag)
    }

    private func updateComicsViewModels(_ viewModels: [ComicsListCellViewModelProtocol]) {
        self.cellViewModels = viewModels
        self.comicsCollectionView.reloadData()
    }

    private func setupStyles() {
        [self.view,
         self.scrollView,
         self.descriptionView,
         self.comicsView,
         self.comicsCollectionView].apply(style: Style().backgroundColor(.app(.background)))

        // TopBar
        self.topBar.apply(style: Style()
                        .tintColor(.raw(.white))
                        .barColor(.app(.background)))

        // Description Section
        self.descriptionTitleLabel.apply(style: Style()
                                            .font(.bold(.larger))
                                            .textColor(.app(.text))
                                            .align(.left))
        self.descriptionValueLabel.apply(style: Style()
                                            .font(.medium(.medium))
                                            .textColor(.app(.text))
                                            .align(.left))

        // Comics Section
        self.comicsTitleLabel.apply(style: Style()
                                        .font(.bold(.larger))
                                        .textColor(.app(.text))
                                        .align(.left))
    }

    // MARK: - Actions
    @IBAction func backButtonIsClicked() {
        self.viewModel.didTapOnBackButton()
    }
}

extension CharacterViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cellViewModels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(for: indexPath, cellType: ComicsListViewCell.self)

        let cellViewModel = self.cellViewModels[indexPath.row]
        cell.setup(viewModel: cellViewModel)

        return cell
    }
}
