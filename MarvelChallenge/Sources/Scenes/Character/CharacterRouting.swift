//
//  CharacterRouting.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 25/10/2020.
//

import Foundation

protocol CharacterRouting: class {

    // MARK: - Functions
    func back()
}
