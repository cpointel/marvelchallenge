//
//  CharacterViewModel.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 25/10/2020.
//

import Foundation
import RxSwift
import RxCocoa

typealias CharacterViewModelServices = HasCharactersService
typealias CharacterViewModelRouting = CharacterRouting

internal struct CharacterViewModelStaticTexts {
    let descriptionSectionTitle: String = L10n.Scene.Character.Description.title
    let comicsSectionTitle: String = L10n.Scene.Character.Comics.title
}

protocol CharacterViewModelProtocol {

    // MARK: - Properties
    var staticTexts: CharacterViewModelStaticTexts { get }
    var thumbnailURL: Driver<URL?> { get }
    var title: Driver<String> { get }
    var description: Driver<String> { get }
    var comicsListViewModels: Driver<[ComicsListCellViewModelProtocol]> { get }
    var showDescription: Driver<Bool> { get }
    var showComics: Driver<Bool> { get }

    // MARK: - Functions
    func didTapOnBackButton()
}

class CharacterViewModel: CharacterViewModelProtocol {

    // MARK: - CharacterViewModelProtocol / Properties
    let staticTexts: CharacterViewModelStaticTexts = CharacterViewModelStaticTexts()
    let thumbnailURL: Driver<URL?>
    let title: Driver<String>
    let description: Driver<String>
    var comicsListViewModels: Driver<[ComicsListCellViewModelProtocol]> { comicsListViewModelsSubject.asDriver(onErrorJustReturn: []) }
    var showDescription: Driver<Bool> { description.map { !$0.isEmpty }.asDriver(onErrorJustReturn: false) }
    var showComics: Driver<Bool> { comicsListViewModels.map { !$0.isEmpty }.asDriver(onErrorJustReturn: false) }

    // MARK: - Subjects
    private let comicsListViewModelsSubject = BehaviorSubject<[ComicsListCellViewModelProtocol]>(value: [])

    // MARK: - Properties
    private let character: Character
    private var services: CharacterViewModelServices
    private weak var routing: CharacterViewModelRouting?
    private let disposeBag = DisposeBag()

    // MARK: - Birth and Death
    init(character: Character,
         services: CharacterViewModelServices,
         routing: CharacterViewModelRouting?) {
        self.character = character
        self.services = services
        self.routing = routing

        self.title = .just(character.name.capitalized)
        self.description = .just(character.description)
        let thumbnailURL: URL?
        if let thumbnail = character.thumbnail {
            thumbnailURL = ImagesService.imageURL(path: thumbnail.path,
                                                       ext: thumbnail.ext,
                                                       type: .landscapeAmazing)
        } else {
            thumbnailURL = nil
        }
        self.thumbnailURL = .just(thumbnailURL)

        setupServicesBindings()
    }

    // MARK: - Setup
    private func setupServicesBindings() {
        self.services.charactersService.comics(for: character.id)
            .map { comicsList -> [Comic] in
                return comicsList.comics
            }
            .map { comics -> [ComicsListCellViewModelProtocol] in
                return comics.map { ComicsListCellViewModel(comic: $0) }
            }
            .subscribe(
                onSuccess: { [weak self] urls in
                    self?.comicsListViewModelsSubject.onNext(urls)
                },
                onError: { _ in
                    // TODO: Error
                })
            .disposed(by: disposeBag)

    }
}

// MARK: - CharacterViewModelProtocol / Functions
extension CharacterViewModel {

    func didTapOnBackButton() {
        self.routing?.back()
    }
}
