//
//  ComicsListCellViewModel.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 26/10/2020.
//

import Foundation
import RxSwift
import RxCocoa

protocol ComicsListCellViewModelProtocol {

    // MARK: - Properties
    var title: String { get }
    var description: String { get }
    var thumbnailURL: URL? { get }
}

final internal class ComicsListCellViewModel: ComicsListCellViewModelProtocol {

    // MARK: - ComicsListCellViewModelProtocol / Properties
    let title: String
    let description: String
    let thumbnailURL: URL?

    // MARK: - Birth and Death
    init(comic: Comic) {
        self.title = comic.title ?? ""
        self.description = comic.description ?? ""

        if let thumbnail = comic.thumbnail {
            self.thumbnailURL = ImagesService.imageURL(path: thumbnail.path,
                                                       ext: thumbnail.ext,
                                                       type: .portraitUncanny)
        } else {
            self.thumbnailURL = nil
        }
    }
}
