//
//  CharactersListViewCell.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 23/10/2020.
//

import Reusable
import AlamofireImage

internal final class CharactersListViewCell: UICollectionViewCell, NibReusable {

    // MARK: - IBOutlets
    @IBOutlet private weak var topView: UIView!
    @IBOutlet private weak var thumbnailImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!

    // MARK: - Birth and Death
    override func awakeFromNib() {
        super.awakeFromNib()
        setupStyle()
    }

    // MARK: - Setup
    private func setupStyle() {
        [contentView, topView, thumbnailImageView, nameLabel].apply(style: Style().backgroundColor(.app(.background)))
        self.topView.apply(style: Style().cornerRadius(4))
        self.nameLabel.apply(style: Style()
                            .font(.medium(.medium))
                            .textColor(.app(.text))
                            .align(.center))
    }

    func setup(viewModel: CharactersListCellViewModelProtocol) {
        self.nameLabel.text = viewModel.name
        if let imageURL = viewModel.thumbnailURL {
            self.thumbnailImageView.image = nil
            self.thumbnailImageView.af_setImage(withURL: imageURL, imageTransition: .crossDissolve(0.33))
        } else {
            self.thumbnailImageView.image = nil
        }
    }
}
