//
//  CharactersListViewModel.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 22/10/2020.
//

import Foundation
import RxSwift
import RxCocoa

typealias CharactersListViewModelServices = HasCharactersService
typealias CharactersListViewModelRouting = CharactersListRouting

internal struct CharactersListStaticTexts {
    let searchPlaceholder: String = L10n.Scene.Characters.SearchBar.placeholder
}

internal enum CharactersListViewType: Equatable {
    case `default`
    case characters(viewModels: [CharactersListCellViewModelProtocol], isMoreResults: Bool)
    case noResult

    static func == (lhs: CharactersListViewType, rhs: CharactersListViewType) -> Bool {
        switch (lhs, rhs) {
        case (`default`, `default`),
             (noResult, noResult),
             (characters, characters):
            return true
        case (`default`, _),
             (noResult, _),
             (characters, _):
            return false
        }
    }
}

internal struct CharactersListConfiguration {

    // MARK: - Constants
    private static let defaultDebounce = DispatchTimeInterval.milliseconds(300)

    // MARK: - Properties
    let debounce: DispatchTimeInterval?

    // MARK: - Birth and Death
    init(debounce: DispatchTimeInterval? = defaultDebounce) {
        self.debounce = debounce
    }
}

protocol CharactersListViewModelProtocol {

    // MARK: - Properties
    var staticTexts: CharactersListStaticTexts { get }
    var viewType: Driver<CharactersListViewType> { get }
    var noResultViewModel: CharactersListNoResultViewModel { get }

    var isLoading: Driver<Bool> { get }
    var isLoadingMore: Driver<Bool> { get }

    // MARK: - Functions
    func viewDidLoad()
    func didTypeKeywords(_ keywords: String?)
    func didCancelSearch()
    func didScrollToItem(at index: Int)
}

internal final class CharactersListViewModel: CharactersListViewModelProtocol {

    // MARK: - CharactersListViewModelProtocol / Properties
    let staticTexts: CharactersListStaticTexts = CharactersListStaticTexts()
    var viewType: Driver<CharactersListViewType> { viewTypeSubject.asDriver(onErrorJustReturn: .default) }
    let noResultViewModel: CharactersListNoResultViewModel = CharactersListNoResultViewModel()

    var isLoading: Driver<Bool> { isLoadingSubject.asDriver(onErrorDriveWith: .never()) }
    var isLoadingMore: Driver<Bool> { isLoadingMoreSubject.asDriver(onErrorDriveWith: .never()) }

    // MARK: - Subjects
    private let viewTypeSubject = BehaviorSubject<CharactersListViewType>(value: .default)
    private let isLoadingSubject = BehaviorSubject<Bool>(value: false)
    private let isLoadingMoreSubject = BehaviorSubject<Bool>(value: false)
    private let keywordsSubject = BehaviorSubject<String?>(value: nil)

    // MARK: - Properties
    private let configuration: CharactersListConfiguration
    private let services: CharactersListViewModelServices
    private weak var routing: CharactersListViewModelRouting?
    private var disposeBag = DisposeBag()
    private var searchCharactersDisposeBag = DisposeBag()
    private var currentOffset: Int = 0
    private var totalCount: Int = NSNotFound
    private let pageLimit: Int = 20
    private var nameStartWith: String?
    private var results: [CharactersListCellViewModelProtocol] = []

    // MARK: - Birth and Death
    init(configuration: CharactersListConfiguration = CharactersListConfiguration(),
         services: CharactersListViewModelServices,
         routing: CharactersListViewModelRouting?) {
        self.configuration = configuration
        self.services = services
        self.routing = routing

        setupBindings()
    }

    // MARK: - Setup
    private func setupBindings() {
        let searchKeywords: Observable<String?>

        if let deboundeInterval = self.configuration.debounce {
            searchKeywords = keywordsSubject
                .debounce(deboundeInterval, scheduler: MainScheduler.instance)
                .distinctUntilChanged()
                .map { $0?.trimmed }
                .do { [weak self] in
                    self?.nameStartWith = $0
                }
        } else {
            searchKeywords = keywordsSubject
                .distinctUntilChanged()
                .map { $0?.trimmed }
                .do { [weak self] in
                    self?.nameStartWith = $0
                }
        }

        searchKeywords
            .subscribe(onNext: { [weak self] _ in
                self?.searchCharacters()
            })
            .disposed(by: disposeBag)
    }
}

// MARK: - Action / FetchCharacters
extension CharactersListViewModel {

    private func searchCharacters() {
        resetPageInfo()
        executeSearchCharacters(isLoadingMore: false)
    }

    private func searchMoreCharacters() {
        self.currentOffset += self.pageLimit
        executeSearchCharacters(isLoadingMore: true)
    }

    private func executeSearchCharacters(isLoadingMore: Bool) {
        startLoading(isLoadingMore: isLoadingMore)
        self.services.charactersService.characters(nameStartsWith: nameStartWith,
                                              atOffset: currentOffset,
                                              limit: pageLimit)
            .do(onSuccess: { [weak self] charactersList in
                self?.currentOffset = charactersList.page.offset
                self?.totalCount = charactersList.page.totalCount
            })
            .map { [weak self] charactersList -> [CharactersListCellViewModelProtocol] in
                guard let strongSelf = self else {
                    return []
                }
                return charactersList.characters.map { CharactersListCellViewModel(with: $0,
                                                                                   routing: strongSelf.routing)
                }
            }
            .map { [weak self] newViewModels -> [CharactersListCellViewModelProtocol] in
                guard let strongSelf = self else {
                    return []
                }
                var cellViewModels: [CharactersListCellViewModelProtocol] = []

                if isLoadingMore {
                    cellViewModels.append(contentsOf: strongSelf.results)
                }

                cellViewModels.append(contentsOf: newViewModels)
                strongSelf.results = cellViewModels

                return cellViewModels
            }
            .map { viewModels -> CharactersListViewType  in
                if viewModels.isEmpty {
                    return CharactersListViewType.noResult
                }
                return CharactersListViewType.characters(viewModels: viewModels, isMoreResults: isLoadingMore)
            }
            .subscribe(
                onSuccess: { [weak self] viewType in
                    self?.viewTypeSubject.onNext(viewType)
                    self?.stopLoading(isLoadingMore: isLoadingMore)
                }, onError: { [weak self] _ in
                    self?.viewTypeSubject.onNext(.default)
                    self?.stopLoading(isLoadingMore: isLoadingMore)
                })
            .disposed(by: disposeBag)
    }

    private func resetPageInfo() {
        self.currentOffset = 0
        self.totalCount = NSNotFound
    }

    private func cancelSearch() {
        self.keywordsSubject.onNext(nil)
    }
}

// MARK: - Loading
extension CharactersListViewModel {

    private func startLoading(isLoadingMore: Bool) {
        if isLoadingMore {
            isLoadingMoreSubject.onNext(true)
        } else {
            isLoadingSubject.onNext(true)
        }
    }

    private func stopLoading(isLoadingMore: Bool) {
        if isLoadingMore {
            isLoadingMoreSubject.onNext(false)
        } else {
            isLoadingSubject.onNext(false)
        }
    }
}

// MARK: - CharactersListViewModelProtocol / Functions
extension CharactersListViewModel {

    func viewDidLoad() {
        searchCharacters()
    }

    func didTypeKeywords(_ keywords: String?) {
        if let keywords = keywords, !keywords.isEmpty {
            self.keywordsSubject.onNext(keywords)
        } else {
            cancelSearch()
        }
    }

    func didCancelSearch() {
        cancelSearch()
    }

    func didScrollToItem(at index: Int) {
        guard let isLoadingMore = try? isLoadingMoreSubject.value(), isLoadingMore == false else {
            return
        }
        let hasMoreCharacters: Bool = (totalCount == NSNotFound) ? false : (currentOffset < totalCount)
        if index > (currentOffset - (pageLimit / 2)) && hasMoreCharacters {
            searchMoreCharacters()
        }
    }
}
