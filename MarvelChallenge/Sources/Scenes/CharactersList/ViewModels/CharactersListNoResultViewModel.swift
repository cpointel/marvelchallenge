//
//  CharactersListNoResultViewModel.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 24/10/2020.
//

import UIKit

internal final class CharactersListNoResultViewModel: PlaceholderViewModel {

    // MARK: - Birth And Death
    init() {
        super.init(title: L10n.Scene.Characters.Placeholder.NoResult.title,
                   image: Asset.Illustrations.illNoResult.image,
                   imageSizeType: .ratio(1),
                   description: L10n.Scene.Characters.Placeholder.NoResult.description)
    }
}
