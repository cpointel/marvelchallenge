//
//  CharactersListCellViewModel.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 23/10/2020.
//

import Foundation

typealias CharactersListCellViewModelRouting = CharactersListRouting

protocol CharactersListCellViewModelProtocol {

    // MARK: - Properties
    var name: String { get }
    var thumbnailURL: URL? { get }

    // MARK: - Functions
    func didTapOnView()
}

internal final class CharactersListCellViewModel: CharactersListCellViewModelProtocol {

    // MARK: - CharactersListCellViewModelProtocol / Properties
    let name: String
    let thumbnailURL: URL?

    // MARK: - Properties
    private weak var routing: CharactersListCellViewModelRouting?
    private let character: Character

    // MARK: - Birth and Death
    init(with character: Character,
         routing: CharactersListCellViewModelRouting?) {
        self.routing = routing
        self.character = character

        self.name = character.name.capitalized
        if let thumbnail = character.thumbnail {
            self.thumbnailURL = ImagesService.imageURL(path: thumbnail.path,
                                                       ext: thumbnail.ext,
                                                       type: .standardXLarge)
        } else {
            self.thumbnailURL = nil
        }
    }
}

// MARK: - CharactersListCellViewModelProtocol / Functions
extension CharactersListCellViewModel {

    func didTapOnView() {
        self.routing?.showCharacter(character)
    }
}
