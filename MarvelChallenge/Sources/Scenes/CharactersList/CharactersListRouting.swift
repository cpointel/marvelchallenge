//
//  CharactersListRouting.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 25/10/2020.
//

import Foundation

protocol CharactersListRouting: class {

    // MARK: - Functions
    func showCharacter(_ character: Character)
}
