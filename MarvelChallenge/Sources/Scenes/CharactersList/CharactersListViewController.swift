//
//  CharactersListViewController.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 22/10/2020.
//

import Reusable
import RxSwift

internal final class CharactersListViewController: UIViewController, StoryboardBased {

    // MARK: - IBOutlets
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private var collectionView: UICollectionView!
    @IBOutlet private weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var loadingMoreIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var containerViewBottomConstraint: NSLayoutConstraint!

    // MARK: - UI
    private var noResultView: PlaceholderView!

    // MARK: - Properties
    private var viewModel: CharactersListViewModelProtocol!
    private var cellViewModels: [CharactersListCellViewModelProtocol] = []
    private let disposeBag = DisposeBag()

    // MARK: - Birth and Death
    static func instantiate(viewModel: CharactersListViewModelProtocol) -> CharactersListViewController {
        let viewController = CharactersListViewController.instantiate()
        viewController.viewModel = viewModel
        viewController.noResultView = PlaceholderView(viewModel: viewModel.noResultViewModel)
        return viewController
    }

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupStaticTexts()
        setupAccessibilityIdentifiers()
        setupSearchBar()
        setupCollectionView()
        setupNoResultView()
        setupStyles()
        setupBindings()
        registerKeyboard()

        self.viewModel.viewDidLoad()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let width = (collectionView.innerWidth() - layout.minimumInteritemSpacing) / 2
            let size = CGSize(width: width, height: width * 1.2)
            layout.itemSize = size
        }
        self.collectionView.collectionViewLayout.invalidateLayout()
    }

    // MARK: - Setup
    private func setupStaticTexts() {
        self.searchBar.placeholder = self.viewModel.staticTexts.searchPlaceholder
    }

    private func setupAccessibilityIdentifiers() {
        self.view.setAccessibility(.sceneCharactersListScreenPage)
        self.searchBar.setAccessibility(.sceneCharactersListSearchBar)
        self.collectionView.setAccessibility(.sceneCharactersListResultsView)
        self.noResultView.setAccessibility(.sceneCharactersListNoResultPlaceholderView)
    }

    private func setupSearchBar() {
        self.searchBar.delegate = self
    }

    private func setupCollectionView() {
        self.collectionView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 16, right: 0)

        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let space: CGFloat = 16
            layout.minimumLineSpacing = space
            layout.minimumInteritemSpacing = space
            layout.sectionInset = UIEdgeInsets(top: space, left: space, bottom: space, right: space)
        }

        self.collectionView.register(cellType: CharactersListViewCell.self)
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
    }

    private func setupNoResultView() {
        let placeholderStyle = Style<PlaceholderView>()
            .titleStyle(Style<UILabel>()
                .font(Font.bold(.larger))
                .textColor(Color.app(.text))
        )
            .descriptionStyle(Style<UILabel>()
                .font(Font.regular(.medium))
                .textColor(Color.app(.secondaryText))
        )
            .actionStyle(Style<UIButton>()
                .font(Font.medium(.medium))
                .textColor(Color.app(.ctaPrimaryText))
                .backgroundColor(.app(.ctaPrimaryBackground))
                .edgeInsets(UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16))
                .cornerRadius(4)
        )

        self.noResultView.apply(style: placeholderStyle)
    }

    private func setupStyles() {
        [self.view, self.collectionView, self.containerView].apply(style: Style().backgroundColor(.app(.background)))
        [self.loadingIndicator, self.loadingMoreIndicator].apply(style: Style().color(.raw(.white)))
    }

    private func setupBindings() {
        self.viewModel.viewType
            .drive(onNext: { [weak self] viewType in
                self?.setupViewType(viewType)
            })
            .disposed(by: disposeBag)

        self.viewModel.isLoading
            .drive(onNext: { [weak self] isLoading in
                if isLoading {
                    self?.loadingIndicator.startAnimating()
                } else {
                    self?.loadingIndicator.stopAnimating()
                }
            })
            .disposed(by: disposeBag)

        self.viewModel.isLoadingMore
            .drive(onNext: { [weak self] isLoading in
                if isLoading {
                    self?.loadingMoreIndicator.startAnimating()
                } else {
                    self?.loadingMoreIndicator.stopAnimating()
                }
            })
            .disposed(by: disposeBag)

        self.collectionView.rx.contentOffset
            .subscribe(onNext: { [weak self] offset in
                guard let collectionView = self?.collectionView else {
                    return
                }
                let limit: CGFloat = 32
                var threshold = collectionView.contentSize.height
                threshold -= collectionView.frame.height
                threshold -= limit
                if offset.y > threshold {
                    let delta = 1 - limit / (offset.y - threshold)
                    self?.loadingMoreIndicator.alpha = min(1, delta)
                } else {
                    self?.loadingMoreIndicator.alpha = 0
                }
            })
            .disposed(by: disposeBag)
    }

    private func setupViewType(_ viewType: CharactersListViewType) {
        self.noResultView?.removeFromSuperview()
        self.collectionView.removeFromSuperview()

        switch viewType {
        case .default:
            break
        case let .characters(viewModels, isMoreResults):
            self.cellViewModels = viewModels
            if !isMoreResults {
                self.collectionView.scrollToTop(animated: false)
            }
            self.containerView.setSubview(collectionView)
            self.collectionView.reloadData()
        case .noResult:
            self.containerView.setSubview(noResultView)
        }
    }
}

// MARK: - UISearchBarDelegate
extension CharactersListViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.viewModel.didTypeKeywords(searchText)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        self.searchBar.text = nil
        self.viewModel.didCancelSearch()
    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = false
    }
}

// MARK: - UICollectionViewDataSource
extension CharactersListViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cellViewModels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(for: indexPath, cellType: CharactersListViewCell.self)

        let cellViewModel = self.cellViewModels[indexPath.row]
        cell.setup(viewModel: cellViewModel)

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.viewModel.didScrollToItem(at: indexPath.row)
    }

    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        self.collectionView.cellForItem(at: indexPath)?.animateScaleDown()
    }

    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        self.collectionView.cellForItem(at: indexPath)?.animateScaleUp()
    }
}

// MARK: - UICollectionViewDelegate
extension CharactersListViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cellViewModel = self.cellViewModels[indexPath.row]
        self.searchBar.resignFirstResponder()
        cellViewModel.didTapOnView()
    }
}

// MARK: - Keyboard management
extension CharactersListViewController {

    private func registerKeyboard() {
        NotificationCenter.default.rx.notification(UIResponder.keyboardWillShowNotification).subscribe(onNext: { [weak self] notification in
            self?.keyboardWillShow(withNotification: notification)
        }).disposed(by: disposeBag)

        NotificationCenter.default.rx.notification(UIResponder.keyboardWillHideNotification).subscribe(onNext: { [weak self] notification in
            self?.keyboardWillHide(withNotification: notification)
        }).disposed(by: disposeBag)
    }

    private func keyboardWillShow(withNotification notification: Notification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue else { return }
        let keyboardViewEndFrame = view.convert(keyboardSize, from: view.window)
        let keyboardHeight = keyboardViewEndFrame.height

        self.containerViewBottomConstraint.constant = keyboardHeight
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutIfNeeded()
        }

    }

    private func keyboardWillHide(withNotification notification: Notification) {
        self.containerViewBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutIfNeeded()
        }
    }
}
