//
//  AppDelegate.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 20/10/2020.
//

import UIKit

var appCore: AppCore = (UIApplication.shared.delegate as? AppDelegate)!.appCore // swiftlint:disable:this force_unwrapping

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    // MARK: - Properties
    var window: UIWindow?
    let appCore = AppCore()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        guard let window = window else {
            fatalError("No Window")
        }
        appCore.setup()
        appCore.start(onWindow: window)
        return true
    }
}
