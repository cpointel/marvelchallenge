//
//  StubsService+MarvelAPI.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 24/10/2020.
//

import Foundation

// swiftlint:disable cyclomatic_complexity
extension StubsService {

    static func loadMarvelApiStubs() {
        let host = "marvel.stub"

        stubCharacters(host: host)
        stubCharacterComics(host: host)
    }

    private static func stubCharacters(host: String) {
        let charactersRequest = StubRequest(host: host, path: "/v1/public/characters")

        load(StubObject(request: charactersRequest,
                        requestValidator: { queryParams, _ in
                            queryParams["nameStartsWith"] == nil
                        },
                        responseHandler: { _, queryParams, _ in
                            let offset = (Int(queryParams["offset"] ?? "0"))
                            switch offset {
                            case 0: return StubResponse(file: .json(name: "marvel_characters_page1"), responseTime: 1)
                            case 20: return StubResponse(file: .json(name: "marvel_characters_page2"), responseTime: 2)
                            case 40: return StubResponse(file: .json(name: "marvel_characters_page3"), responseTime: 2)
                            default: return StubResponse(file: .json(name: "marvel_characters_no_result"))
                            }
                        }))

        load(StubObject(request: charactersRequest,
                        requestValidator: { queryParams, _ in
                            queryParams["nameStartsWith"] != nil
                        },
                        responseHandler: { _, queryParams, _ in
                            let nameStartsWith = (queryParams["nameStartsWith"] ?? "").lowercased()
                            let offset = (Int(queryParams["offset"] ?? "0"))

                            switch (nameStartsWith, offset) {
                            case ("i", 0): return StubResponse(file: .json(name: "marvel_characters_search_i_page1"), responseTime: 1)
                            case ("i", 20): return StubResponse(file: .json(name: "marvel_characters_search_i_page2"), responseTime: 1)
                            case ("ir", 0): return StubResponse(file: .json(name: "marvel_characters_search_ir_page1"), responseTime: 1)
                            case ("iro", 0): return StubResponse(file: .json(name: "marvel_characters_search_iro_page1"), responseTime: 1)
                            case ("iron", 0): return StubResponse(file: .json(name: "marvel_characters_search_iron_page1"), responseTime: 1)
                            case ("iron m", 0): return StubResponse(file: .json(name: "marvel_characters_search_iron_m_page1"), responseTime: 1)
                            case ("iron ma", 0): return StubResponse(file: .json(name: "marvel_characters_search_iron_ma_page1"), responseTime: 1)
                            default: return StubResponse(file: .json(name: "marvel_characters_no_result"))
                            }
                        }))
    }

    private static func stubCharacterComics(host: String) {
        load(StubObject(request: StubRequest(host: host, path: "/v1/public/characters/1009144/comics"),
                        response: StubResponse(file: .json(name: "marvel_characters_comics"))))
        load(StubObject(request: StubRequest(host: host, path: "/v1/public/characters/1011334/comics"),
                        response: StubResponse(file: .json(name: "marvel_characters_comics"))))
    }
}
