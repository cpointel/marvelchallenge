//
//  StubsService+Images.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 24/10/2020.
//

import Foundation

extension StubsService {

    // swiftlint:disable cyclomatic_complexity
    static func loadImageStubs() {
        let host = "i.annihil.us"
        load(StubObject(
            urlValidator: { req -> Bool in
                let isHostValid = req.url?.host == host
                let isStardardXLargeValid = req.url?.absoluteString.hasSuffix("/standard_xlarge.jpg") ?? false
                let isPortraitUncannyValid = req.url?.absoluteString.hasSuffix("/portrait_uncanny.jpg") ?? false
                let isLandscapeAmazingValid = req.url?.absoluteString.hasSuffix("/landscape_amazing.jpg") ?? false
                    return isHostValid && (isStardardXLargeValid || isPortraitUncannyValid || isLandscapeAmazingValid)
            }, responseHandler: { path, _, _ in
                let identifier = path?.components(separatedBy: "/")[8]
                let digits: [Int]? = identifier?.compactMap(\.wholeNumberValue)
                if let digit: Int = digits?.last {
                    let responseTime = TimeInterval(Float.random(in: 0 ..< 1.5))
                    switch digit {
                    case 1: return StubResponse(file: .png(name: "jordan_donkey"), responseTime: 5)
                    case 2: return StubResponse(file: .png(name: "jordan_monastery"), responseTime: responseTime)
                    case 3: return StubResponse(file: .png(name: "jordan_wadirum_sunset"), responseTime: responseTime)
                    case 4: return StubResponse(file: .png(name: "jordan_wadirum_surf"), responseTime: responseTime)
                    case 5: return StubResponse(file: .png(name: "mauritius_island"), responseTime: responseTime)
                    case 6: return StubResponse(file: .png(name: "mauritius_lotus"), responseTime: responseTime)
                    case 7: return StubResponse(file: .png(name: "mauritius_turtle"), responseTime: responseTime)
                    case 8: return StubResponse(file: .png(name: "tanzania_gazelle"), responseTime: responseTime)
                    case 9: return StubResponse(file: .png(name: "tanzania_giraffe"), responseTime: responseTime)
                    default: return StubResponse(file: .png(name: "tanzania_lizard"), responseTime: responseTime)
                    }
                } else {
                    return StubResponse(file: .png(name: "jordan_donkey"))
                }
            }))
    }
}
