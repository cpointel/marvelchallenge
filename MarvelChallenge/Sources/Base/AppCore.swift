//
//  AppCore.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 23/10/2020.
//

import UIKit

#if DEBUG
internal let defaultLogger = Logger(logger: { print($0) },
                                    level: .debug,
                                    format: .dev,
                                    topicLevels: [
                                        .application: .info,
                                        .network: .debug
])
#else
internal let defaultLogger = Logger(logger: { _ in },
                                    level: .off)
#endif

class AppCore {

    // MARK: - Properties
    var businessServices: BusinessServicesProtocol!
    private let charactersCoordinator: Coordinator

    // MARK: - Birth and Death
    init() {
        charactersCoordinator = CharactersCoordinator(navigationController: UINavigationController())
    }

    // MARK: - Setup
    func setup() {
        setupServices()

        #if DEBUG
        setupStubs()
        #endif
    }

    private func setupServices() {
        guard let services = BusinessServices() else {
            Logger.logFatal("Can't initialize 'BusinessServices'")
            fatalError()
        }
        self.businessServices = services
    }

    private func setupStubs() {
        StubsService.loadUnknownRequestStubs()
        StubsService.loadMarvelApiStubs()
        StubsService.loadImageStubs()
    }

    // MARK: - Action
    func start(onWindow window: UIWindow) {
        charactersCoordinator.start()
        let controller = charactersCoordinator.controller
        window.tintColor = RawColor.black.color
        window.rootViewController = controller
        window.makeKeyAndVisible()
    }
}
