//
//  UICollectionView+Extensions.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 23/10/2020.
//

import UIKit

extension UICollectionView {

    public func innerWidth() -> CGFloat {
        var width = frame.width
        width -= contentInset.left
        width -= contentInset.right

        if let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout {
            width -= flowLayout.sectionInset.left
            width -= flowLayout.sectionInset.right
        }

        return width
    }

    public func setCellSpacing(to space: CGFloat) {
        if let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.minimumInteritemSpacing = space
            flowLayout.minimumLineSpacing = space
        }
    }
}
