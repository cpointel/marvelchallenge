//
//  UIScrollView+Extensions.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 23/10/2020.
//

import UIKit

extension UIScrollView {

    public func scrollToTop(animated: Bool = true) {
        if let tableView = self as? UITableView, tableView.estimatedRowHeight != 0, animated {
            if tableView.numberOfSections > 0 {
                tableView.scrollToRow(at: IndexPath(row: NSNotFound, section: 0), at: .top, animated: animated)
            }
        } else {
            let top = CGPoint(x: contentOffset.x, y: -contentInset.top)
            setContentOffset(top, animated: animated)
        }
    }
}
