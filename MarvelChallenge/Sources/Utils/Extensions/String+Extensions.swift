//
//  String+Extensions.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 23/10/2020.
//

import Foundation

extension String {

    public var trimmed: String {
      return trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
}
