//
//  Encodable+Extensions.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 21/10/2020.
//

import Foundation

extension Encodable {

    public var JSONDictionary: [String: Any] {
        do {
            return try JSONSerialization.jsonObject(with: JSONEncoder().encode(self),
                                                    options: .allowFragments) as? [String: Any] ?? [:]
        } catch { return [:] }
    }
}

extension Encodable {

    public func toJSONData() -> Data? {
        return try? JSONEncoder().encode(self)
    }
}
