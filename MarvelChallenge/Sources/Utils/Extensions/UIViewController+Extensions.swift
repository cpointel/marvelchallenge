//
//  UIViewController+Extensions.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 26/10/2020.
//

import UIKit

extension UIViewController {

    func addChildViewController(viewController: UIViewController, containerView: UIView) {
        viewController.willMove(toParent: self)
        self.addChild(viewController)
        containerView.setSubview(viewController.view)
        viewController.didMove(toParent: self)
    }

    func removeFromSuperviewAndParentViewController() {
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
}
