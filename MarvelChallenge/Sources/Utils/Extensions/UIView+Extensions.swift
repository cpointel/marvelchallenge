//
//  UIView+Extensions.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 23/10/2020.
//

import UIKit

public extension UIView {

    func setSubview(_ view: UIView, edges: UIEdgeInsets = .zero, atIndex index: Int? = nil) {
        // Add the subview
        if let index = index {
            insertSubview(view, at: index)
        } else {
            addSubview(view)
        }

        // Add constraints
        view.translatesAutoresizingMaskIntoConstraints = false
        addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-(\(edges.left))-[view]-(\(edges.right))-|",
            metrics: nil,
            views: ["view": view]))
        addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-(\(edges.top))-[view]-(\(edges.bottom))-|",
            metrics: nil,
            views: ["view": view]))
    }
}

public extension UIView {

    func animateScaleDown(ratio: CGFloat = 0.95) {
        UIView.animate(withDuration: 0.1) { [weak self] in
            self?.layer.transform = CATransform3DMakeScale(ratio, ratio, 1.0)
        }
    }

    func animateScaleUp() {
        UIView.animate(withDuration: 0.1) { [weak self] in
            self?.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
        }
    }
}
