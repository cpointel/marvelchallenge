//
//  PlaceholderViewModel.swif
//
//  Created by Cedric Pointel on 19/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import UIKit

public protocol PlaceholderViewModelProtocol {

    // MARK: - Properties
    var title: String { get }
    var image: UIImage? { get }
    var imageSizeType: PlaceholderImageSizeType { get }
    var description: String { get }
    var actionTitle: String? { get }

    // MARK: - Functions
    func didClickOnAction()
}

public enum PlaceholderImageSizeType: Equatable {
    case none
    case size(CGSize)
    case ratio(CGFloat)
}

public class PlaceholderViewModel: PlaceholderViewModelProtocol {

    // MARK: - PlaceholderViewModelProtocol / Properties
    public let title: String
    public let image: UIImage?
    public let imageSizeType: PlaceholderImageSizeType
    public let description: String
    public let actionTitle: String?
    public let action: (() -> Void)?

    // MARK: - Birth and Death
    public init(title: String,
                image: UIImage?,
                imageSizeType: PlaceholderImageSizeType = .none,
                description: String,
                actionTitle: String,
                action: @escaping (() -> Void)) {
        self.title = title
        self.image = image
        self.imageSizeType = imageSizeType
        self.description = description
        self.actionTitle = actionTitle
        self.action = action
    }

    public init(title: String,
                image: UIImage?,
                imageSizeType: PlaceholderImageSizeType = .none,
                description: String) {
        self.title = title
        self.image = image
        self.imageSizeType = imageSizeType
        self.description = description
        self.actionTitle = nil
        self.action = nil
    }

    // MARK: - PlaceholderViewModelProtocol / Functions
    public func didClickOnAction() {
        action?()
    }
}
