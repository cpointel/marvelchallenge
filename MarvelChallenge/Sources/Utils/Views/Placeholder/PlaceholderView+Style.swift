//
//  PlaceholderView+Style.swift
//
//  Created by Cedric Pointel on 19/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import UIKit

internal extension Style where T: PlaceholderView {

    func titleStyle(_ style: Style<UILabel>) -> StyleBlock {
        return combine(Style { $0.titleStyle = style })
    }

    func descriptionStyle(_ style: Style<UILabel>) -> StyleBlock {
        return combine(Style { $0.descriptionStyle = style })
    }

    func actionStyle(_ style: Style<UIButton>) -> StyleBlock {
        return combine(Style { $0.actionStyle = style })
    }
}
