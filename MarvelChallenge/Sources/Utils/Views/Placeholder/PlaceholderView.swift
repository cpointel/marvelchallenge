//
//  PlaceholderView.swift
//
//  Created by Cedric Pointel on 19/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import Reusable

class PlaceholderView: UIView, NibOwnerLoadable {

    // MARK: - IBOutlets
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var actionButton: UIButton!
    @IBOutlet private weak var actionView: UIView!
    @IBOutlet private weak var imageWidthConstraint: NSLayoutConstraint!
    @IBOutlet private weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var imageRatioConstraint: NSLayoutConstraint!

    // MARK: - UI Settings
    var titleStyle: Style<UILabel> = .init() {
        didSet { self.titleLabel.apply(style: titleStyle) }
    }
    var descriptionStyle: Style<UILabel> = .init() {
        didSet { self.descriptionLabel.apply(style: descriptionStyle) }
    }
    var actionStyle: Style<UIButton> = .init() {
        didSet { self.actionButton.apply(style: actionStyle) }
    }

    // MARK: - Properties
    private var viewModel: PlaceholderViewModelProtocol!

    // MARK: - Birth and Death
    convenience init(viewModel: PlaceholderViewModelProtocol) {
        self.init(frame: .zero)
        self.loadNibContent()
        self.viewModel = viewModel
        setupBinding()
    }

    // MARK: - Setup
    private func setupBinding() {
        self.titleLabel.text = self.viewModel.title
        self.imageView.image = self.viewModel.image
        self.descriptionLabel.text = self.viewModel.description
        self.actionButton.setTitle(self.viewModel.actionTitle, for: .normal)
        self.actionView.isHidden = self.viewModel.actionTitle == nil

        self.imageWidthConstraint.isActive = false
        self.imageHeightConstraint.isActive = false
        self.imageRatioConstraint.isActive = false

        switch viewModel.imageSizeType {
        case .none:
            break
        case .size(let size):
            self.imageWidthConstraint.constant = size.width
            self.imageWidthConstraint.isActive = true
            self.imageHeightConstraint.constant = size.height
            self.imageHeightConstraint.isActive = true
        case .ratio(let ratio):
            self.imageRatioConstraint.constant = ratio
            self.imageRatioConstraint.isActive = true
        }
    }

    // MARK: - Actions
    @IBAction private func isActionButtonClicked(_ sender: Any?) {
        self.viewModel.didClickOnAction()
    }
}
