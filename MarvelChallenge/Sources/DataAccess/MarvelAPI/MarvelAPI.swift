//
//  MarvelAPI.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 21/10/2020.
//

import Foundation
import RxSwift

private struct Route {
    let path: String
}

protocol MarvelApiProtocol {

    // MARK: - Functions
    func fetchCharacters(dto: APICharactersRequestDTO) -> Single<APIDataWrapperDTO<APICharacterDTO>>
    func fetchComics(for characterId: Int, dto: APIComicsRequestDTO) -> Single<APIDataWrapperDTO<APIComicDTO>>
}

class MarvelApi: MarvelApiProtocol {

    // MARK: - Paths
    private enum Routes {
        case characters
        case comics(characterId: Int)

        var value: Route {
            switch self {
            case .characters: return Route(path: "/v1/public/characters")
            case let .comics(characterId): return Route(path: "/v1/public/characters/\(characterId)/comics")
            }
        }
    }

    // MARK: - Properties
    private let networker: APINetworkerProtocol
    private let apiKey: String
    private let timestamp: String
    private let hash: String
    private let disposeBag = DisposeBag()

    // MARK: - Birth and Death
    init(networker: APINetworkerProtocol, apiKey: String, timestamp: String, hash: String) {
        Logger.logInfo("Init MarvelAPI", for: .application)

        self.networker = networker
        self.apiKey = apiKey
        self.timestamp = timestamp
        self.hash = hash
    }

    // MARK: - Utils
    private func constructRequest(method: APINetworkerRequest.Method = APINetworkerRequest.Method.get,
                                  route: MarvelApi.Routes,
                                  headers: APINetworkerRequest.Headers = [:],
                                  parameters: APINetworkerRequest.Parameters = [:],
                                  encoding: APINetworkerRequest.ParameterEncoding = APINetworkerRequest.ParameterEncoding.json) -> APINetworkerRequest {
        var parameters = parameters
        parameters["apikey"] = self.apiKey
        parameters["ts"] = self.timestamp
        parameters["hash"] = self.hash

        return APINetworkerRequest(method: method,
                                   path: route.value.path,
                                   headers: headers,
                                   parameters: parameters,
                                   encoding: encoding)
    }
}

// MARK: - Characters
extension MarvelApi {

    func fetchCharacters(dto: APICharactersRequestDTO) -> Single<APIDataWrapperDTO<APICharacterDTO>> {
        let request = constructRequest(method: .get,
                                       route: .characters,
                                       parameters: dto.JSONDictionary)
        return networker.execute(request: request)
            .asSingle()
    }

    func fetchComics(for characterId: Int, dto: APIComicsRequestDTO) -> Single<APIDataWrapperDTO<APIComicDTO>> {
        let request = constructRequest(method: .get,
                                       route: .comics(characterId: characterId),
                                       parameters: dto.JSONDictionary)
        return networker.execute(request: request)
            .asSingle()
    }
}
