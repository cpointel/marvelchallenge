//
//  APIDataContainerDTO.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 21/10/2020.
//

import Foundation

struct APIDataContainerDTO<T: Decodable>: Decodable {

    // MARK: - Properties
    let offset: Int
    let limit: Int
    let total: Int
    let count: Int
    let results: [T]
}
