//
//  APIDataWrapperDTO.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 21/10/2020.
//

import Foundation

struct APIDataWrapperDTO<T: Decodable>: Decodable {

    // MARK: - Properties
    let code: Int
    let data: APIDataContainerDTO<T>
}
