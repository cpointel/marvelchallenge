//
//  APIComicDTO.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 26/10/2020.
//

import Foundation

struct APIComicDTO: Decodable {

    enum CodingKeys: String, CodingKey {
        case id
        case title
        case description
        case thumbnail
    }

    // MARK: - Properties
    let id: Int
    let title: String?
    let description: String?
    let thumbnail: APIThumbnailDTO?
}
