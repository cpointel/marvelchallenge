//
//  APIComicsRequestDTO.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 26/10/2020.
//

import Foundation

struct APIComicsRequestDTO: Encodable {

    enum CodingKeys: String, CodingKey {
        case offset
        case limit
    }

    // MARK: - Properties
    let offset: Int
    let limit: Int

    // MARK: - Birth and Death
    init(atOffset offset: Int,
         limit: Int) {
        self.offset = offset
        self.limit = limit
    }
}
