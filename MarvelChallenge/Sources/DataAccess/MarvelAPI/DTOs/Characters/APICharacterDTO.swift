//
//  APICharacterDTO.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 21/10/2020.
//

import Foundation

struct APICharacterDTO: Decodable {

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case thumbnail
    }

    // MARK: - Properties
    let id: Int
    let name: String
    let description: String
    let thumbnail: APIThumbnailDTO
}
