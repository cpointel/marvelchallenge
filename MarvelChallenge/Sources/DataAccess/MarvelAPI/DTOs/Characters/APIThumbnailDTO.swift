//
//  APIThumbnailDTO.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 22/10/2020.
//

import Foundation

struct APIThumbnailDTO: Decodable {

    enum CodingKeys: String, CodingKey {
        case path
        case ext = "extension"
    }

    // MARK: - Properties
    let path: String
    let ext: String
}
