//
//  APICharactersRequestDTO.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 21/10/2020.
//

import Foundation

struct APICharactersRequestDTO: Encodable {

    enum CodingKeys: String, CodingKey {
        case nameStartsWith
        case offset
        case limit
        case orderBy
    }

    // MARK: - Properties
    let nameStartsWith: String?
    let offset: Int
    let limit: Int
    let orderBy: APICharactersOrderByDTO

    // MARK: - Birth and Death
    init(nameStartsWith: String?,
         atOffset offset: Int,
         limit: Int,
         orderBy: APICharactersOrderByDTO = .ascName) {
        self.nameStartsWith = nameStartsWith
        self.offset = offset
        self.limit = limit
        self.orderBy = orderBy
    }
}
