//
//  APICharactersOrderByTypeDTO.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 22/10/2020.
//

import Foundation

enum APICharactersOrderByDTO: String, Encodable {
    case ascName = "name"
    case dscName = "-name"
    case ascModified = "modified"
    case dscModified = "-modified"
}
