//
//  Config.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 21/10/2020.
//

import Foundation

private enum InfoDictionaryBridge {
    enum Error: Swift.Error {
        case missingKey, invalidValue
    }

    static func value<T>(for key: String) throws -> T where T: LosslessStringConvertible {
        guard let object = Bundle.main.object(forInfoDictionaryKey: key) else {
            throw Error.missingKey
        }

        switch object {
        case let value as T:
            return value
        case let string as String:
            guard let value = T(string) else { fallthrough }
            return value
        default:
            throw Error.invalidValue
        }
    }
}

// swiftlint:disable force_unwrapping
// swiftlint:disable force_try
enum Config {

    enum Keys: String {
        case apiUrl = "API_URL"
        case version = "CFBundleShortVersionString"
        case build = "CFBundleVersion"
    }

    static var apiURL: URL {
        return try! URL(string: InfoDictionaryBridge.value(for: Keys.apiUrl.rawValue))!
    }

    static var appVersion: String {
        return try! InfoDictionaryBridge.value(for: Keys.version.rawValue)
    }

    static var buildNumber: String {
        return try! InfoDictionaryBridge.value(for: Keys.build.rawValue)
    }
}
