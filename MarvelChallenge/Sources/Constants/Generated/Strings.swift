// swiftlint:disable all
// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation


public enum L10n {
  private static let noTranslation = "NO_TRANSLATION"

  public enum Application {
    /// Marvel Challenge
    public static let name = L10n.tr(table: "Localizable", "application.name")
  }

  public enum Scene {

    public enum Character {

      public enum Comics {
        /// Comics
        public static let title = L10n.tr(table: "Localizable", "scene.character.comics.title")
      }

      public enum Description {
        /// Description
        public static let title = L10n.tr(table: "Localizable", "scene.character.description.title")
      }
    }

    public enum Characters {

      public enum Placeholder {

        public enum NoResult {
          /// Modify your keywords
          public static let description = L10n.tr(table: "Localizable", "scene.characters.placeholder.noResult.description")
          /// No character
          public static let title = L10n.tr(table: "Localizable", "scene.characters.placeholder.noResult.title")
        }
      }

      public enum SearchBar {
        /// Iron Man, Wolverine, ...
        public static let placeholder = L10n.tr(table: "Localizable", "scene.characters.searchBar.placeholder")
      }
    }
  }
}

extension L10n {
  public static func tr(table: String? = nil, _ key: String, _ args: CVarArg...) -> String {
    var format = NSLocalizedString(key,
                                   bundle: Bundle.main,
                                   value: NSLocalizedString(key, bundle: Bundle(for: BundleToken.self), value: L10n.noTranslation, comment: ""),
                                   comment: "")
    // Fallback to Base localization
    if format == L10n.noTranslation,
       let sportBundlePath = Bundle(for: BundleToken.self).path(forResource: "Base", ofType: "lproj"),
       let sportLanguageBundle = Bundle(path: sportBundlePath) {
      format = NSLocalizedString(key, bundle: sportLanguageBundle, comment: "")
    }
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
