//
//  AccessibilityIdentifiers.swift
//  MarvelChallengeUITests
//
//  Created by Cedric Pointel on 25/10/2020.
//

import Foundation

enum AccessibilityIdentifiers: String {

    // CharactersList
    case sceneCharactersListScreenPage
    case sceneCharactersListSearchBar
    case sceneCharactersListNoResultPlaceholderView
    case sceneCharactersListResultsView

    // Character
    case sceneCharacterScreenPage
    case sceneCharacterDescriptionView
    case sceneCharacterComicsView
}

import UIKit

internal extension UIView {
    func setAccessibility(_ accessibility: AccessibilityIdentifiers) {
        accessibilityIdentifier = accessibility.rawValue
    }
}
internal extension UITabBarItem {
    func setAccessibility(_ accessibility: AccessibilityIdentifiers) {
        accessibilityIdentifier = accessibility.rawValue
    }
}
