//
//  Coordinator.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 23/10/2020.
//

import UIKit

protocol Coordinator {

    // MARK: - Properties
    var controller: UIViewController { get }

    // MARK: - Functions
    func start()
}
