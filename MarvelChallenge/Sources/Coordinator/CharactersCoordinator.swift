//
//  CharactersCoordinator.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 23/10/2020.
//

import UIKit

internal final class CharactersCoordinator: Coordinator {

    // MARK: - Coordinator / Properties
    var controller: UIViewController { navigationController }

    // MARK: - Properties
    private let navigationController: UINavigationController

    // MARK: - Birth and Death
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let viewModel = CharactersListViewModel(services: appCore.businessServices,
                                                routing: self)
        let viewController = CharactersListViewController.instantiate(viewModel: viewModel)
        navigationController.setNavigationBarHidden(true, animated: false)
        navigationController.navigationBar.isTranslucent = true
        navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController.navigationBar.shadowImage = UIImage()
        navigationController.viewControllers = [viewController]
    }
}

// MARK: - CharactersListRouting
extension CharactersCoordinator: CharactersListRouting {

    func showCharacter(_ character: Character) {
        let viewModel = CharacterViewModel(character: character,
                                           services: appCore.businessServices,
                                           routing: self)
        let viewController = CharacterViewController.instantiate(viewModel: viewModel)
        navigationController.pushViewController(viewController, animated: true)
    }
}

// MARK: - CharacterRouting
extension CharactersCoordinator: CharacterRouting {

    func back() {
        navigationController.popViewController(animated: true)
    }
}
