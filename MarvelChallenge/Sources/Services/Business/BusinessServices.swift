//
//  BusinessServices.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 21/10/2020.
//

import Foundation
import Swinject

typealias BusinessServicesProtocol = HasCharactersService

// swiftlint:disable force_unwrapping
internal class BusinessServices: BusinessServicesProtocol {

    // MARK: - Swinject
    private let container = Container()

    // MARK: - DI
    var charactersService: CharactersServiceProtocol { return container.resolve(CharactersService.self)! }

    // MARK: - Properties
    private let marvelApi: MarvelApi

    // Birth and Death
    init?() {
        guard let networker = APINetworker.afNetworker(url: Config.apiURL) else { return nil }
        marvelApi = MarvelApi(networker: networker,
                              apiKey: "7ce589f4c0bde74247bff58e278df3fa",
                              timestamp: "Europe/Paris",
                              hash: "de847ba411141ae3abaaa908b464f174")

        container.register(CharactersService.self) { _ in
            return CharactersService(dataAccess: self.marvelApi)
        }.inObjectScope(.container)
    }
}

internal protocol HasCharactersService {
    var charactersService: CharactersServiceProtocol { get }
}
