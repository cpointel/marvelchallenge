//
//  CharactersService.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 21/10/2020.
//

import Foundation
import RxSwift

internal protocol CharactersServiceProtocol {

    // MARK: - Functions
    func characters(nameStartsWith: String?, atOffset offset: Int, limit: Int) -> Single<CharactersList>
    func comics(for characterId: Int) -> Single<ComicsList>
}

final internal class CharactersService: CharactersServiceProtocol {

    // MARK: - Properties
    private let dataAccess: MarvelApiProtocol

    // MARK: - Birth and Death
    init(dataAccess: MarvelApiProtocol) {
        self.dataAccess = dataAccess
    }
}

// MARK: - CharactersServiceProtocol / Functions
extension CharactersService {

    func characters(nameStartsWith: String?, atOffset offset: Int, limit: Int) -> Single<CharactersList> {
        let dto = APICharactersRequestDTO(nameStartsWith: nameStartsWith,
                                          atOffset: offset,
                                          limit: limit)
        return self.dataAccess.fetchCharacters(dto: dto)
            .map { return CharactersService.convertToCharactersList(dto: $0) }
    }

    func comics(for characterId: Int) -> Single<ComicsList> {
        // We force only to the first 20 comics
        let dto = APIComicsRequestDTO(atOffset: 0,
                                      limit: 20)
        return self.dataAccess.fetchComics(for: characterId, dto: dto)
            .map { return CharactersService.convertToComicsList(dto: $0) }
    }
}

// MARK: - Mappers / Characters
extension CharactersService {

    private static func convertToCharactersList(dto: APIDataWrapperDTO<APICharacterDTO>) -> CharactersList {
        let characters = dto.data.results.map { CharactersService.convertToCharacter(dto: $0) }
        let page = CharactersService.convertToPage(dto: dto.data)
        return CharactersList(characters: characters, page: page)
    }

    private static func convertToCharacter(dto: APICharacterDTO) -> Character {
        return Character(id: dto.id,
                         name: dto.name,
                         description: dto.description,
                         thumbnail: convertToThumbnail(dto: dto.thumbnail))
    }
}

// MARK: - Mappers / Comics
extension CharactersService {

    private static func convertToComicsList(dto: APIDataWrapperDTO<APIComicDTO>) -> ComicsList {
        let comics = dto.data.results.map { CharactersService.convertToComic(dto: $0) }
        let page = CharactersService.convertToPage(dto: dto.data)
        return ComicsList(comics: comics, page: page)
    }

    private static func convertToComic(dto: APIComicDTO) -> Comic {
        return Comic(id: dto.id,
                     title: dto.title,
                     description: dto.description,
                     thumbnail: convertToThumbnail(dto: dto.thumbnail))
    }
}

// MARK: - Mappers / Thumbnail
extension CharactersService {

    private static func convertToThumbnail(dto: APIThumbnailDTO?) -> Thumbnail? {
        guard let dto = dto else {
            return nil
        }
        return Thumbnail(path: dto.path,
                         ext: dto.ext)
    }
}

// MARK: - Mappers / Page
extension CharactersService {

    private static func convertToPage(dto: APIDataContainerDTO<APICharacterDTO>) -> Page {
        return Page(offset: dto.offset,
                    count: dto.count,
                    totalCount: dto.total)
    }

    private static func convertToPage(dto: APIDataContainerDTO<APIComicDTO>) -> Page {
        return Page(offset: dto.offset,
                    count: dto.count,
                    totalCount: dto.total)
    }
}
