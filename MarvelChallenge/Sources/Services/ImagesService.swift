//
//  ImagesService.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 23/10/2020.
//

import Foundation

enum ImageType: String {

    // MARK: - Cases
    case standardMedium = "standard_medium"
    case standardXLarge = "standard_xlarge"
    case portraitUncanny = "portrait_uncanny"
    case landscapeAmazing = "landscape_amazing"
}

protocol ImagesServiceProtocol {

    // MARK: - Functions
    static func imageURL(path: String, ext: String, type: ImageType) -> URL?
}

class ImagesService: ImagesServiceProtocol {}

extension ImagesService {

    static func imageURL(path: String, ext: String, type: ImageType) -> URL? {
        let imageURL = "\(path)/\(type.rawValue).\(ext)"
        return URL(string: imageURL)
    }
}
