//
//  Page.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 23/10/2020.
//

import Foundation

struct Page: Equatable {

    // MARK: - Properties
    let offset: Int
    let count: Int
    let totalCount: Int
}
