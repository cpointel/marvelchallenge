//
//  Thumbnail.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 22/10/2020.
//

import Foundation

struct Thumbnail: Equatable {

    // MARK: - Properties
    let path: String
    let ext: String
}
