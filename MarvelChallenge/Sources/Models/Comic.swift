//
//  Comic.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 26/10/2020.
//

import Foundation

struct Comic: Equatable {

    // MARK: - Properties
    let id: Int
    let title: String?
    let description: String?
    let thumbnail: Thumbnail?
}
