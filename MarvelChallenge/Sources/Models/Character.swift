//
//  Character.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 21/10/2020.
//

import Foundation

struct Character: Equatable {

    // MARK: - Properties
    let id: Int
    let name: String
    let description: String
    let thumbnail: Thumbnail?
}
