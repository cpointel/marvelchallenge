//
//  CharactersList.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 23/10/2020.
//

import Foundation

struct CharactersList: Equatable {

    // MARK: - Properties
    let characters: [Character]
    let page: Page
}
