//
//  ComicsList.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 26/10/2020.
//

import Foundation

struct ComicsList: Equatable {

    // MARK: - Properties
    let comics: [Comic]
    let page: Page
}
