//
//  StubsService.swift
//
//  Created by Cedric Pointel on 22/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import Foundation
import OHHTTPStubs

struct StubsService {

    static func loadUnknownRequestStubs() {
        stub(condition: { req -> Bool in
            return true
        }, response: { req -> HTTPStubsResponse in
            let errorMessage = "The request \(req) was not stubbed and would have hit the real network"
            let error = NSError(domain: NSURLErrorDomain,
                                code: URLError.badURL.rawValue,
                                userInfo: [NSLocalizedDescriptionKey: errorMessage])
            return HTTPStubsResponse(error: error)
        })
    }

    static func load(_ stubObject: StubObjectProtocol) {
        stub(condition: { req -> Bool in
            return stubObject.validate(req: req)
        }, response: { req -> HTTPStubsResponse in
            let response = stubObject.response(for: req)
            guard response.statusCode > 0 else {
                let error = NSError(domain: NSURLErrorDomain, code: Int(response.statusCode))
                return HTTPStubsResponse(error: error)
            }

            let stubsResponse = HTTPStubsResponse(fileAtPath: response.file.path,
                                     statusCode: response.statusCode,
                                     headers: response.headers)
            stubsResponse.responseTime(response.responseTime)
            return stubsResponse
        })
    }
}
