//
//  StubResponse.swift
//
//  Created by Cedric Pointel on 26/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import Foundation

struct StubResponse {

    // MARK: - Properties
    let statusCode: Int32
    let headers: [String: String]
    let file: StubFile
    let responseTime: TimeInterval

    // MARK: - Birth and Death
    init(statusCode: Int32 = 200, file: StubFile, responseTime: TimeInterval = 0) {
        self.statusCode = statusCode
        self.file = file
        self.responseTime = responseTime

        switch file.type {
        case .json:
            headers = ["Content-Type": "application/json; charset=UTF-8"]
        case .png:
            headers = ["Content-Type": "image/jpeg"]
        }
    }
}
