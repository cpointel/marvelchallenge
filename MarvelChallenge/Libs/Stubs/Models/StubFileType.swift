//
//  StubFileType.swift
//
//  Created by Cedric Pointel on 26/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import Foundation

private class BundleToken {}

struct StubFile {

    // MARK: - Type
    enum FileType: String {
        case json
        case png
    }

    // MARK: - Properties
    let name: String
    let type: StubFile.FileType
    let inDirectory: String

    // MARK: - Computed
    var path: String {
        guard let path = Bundle(for: BundleToken.self).path(forResource: name, ofType: type.rawValue, inDirectory: inDirectory) else {
            // Unable to find the file
            return ""
        }
        return path
    }

    // MARK: - Factories
    static func json(name: String, inDirectory: String = "Fixtures/json") -> StubFile {
        return StubFile(name: name, type: .json, inDirectory: inDirectory)
    }

    static func png(name: String, inDirectory: String = "Fixtures/Images") -> StubFile {
        return StubFile(name: name, type: .png, inDirectory: inDirectory)
    }
}
