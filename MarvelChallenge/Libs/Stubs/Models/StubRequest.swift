//
//  StubRequest.swift
//
//  Created by Cedric Pointel on 26/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import Foundation

struct StubRequest {

    // MARK: - Type
    enum Method: String {
        case get = "GET"
        case post = "POST"
        case put = "PUT"
        case delete = "DELETE"
    }

    // MARK: - Properties
    let host: String
    let method: StubRequest.Method
    let path: String

    // MARK: - Birth and Death
    init(host: String,
         method: StubRequest.Method = .get,
         path: String) {
        self.host = host
        self.method = method
        self.path = path
    }
}
