//
//  StubJsonObject.swift
//
//  Created by Cedric Pointel on 26/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import Foundation

protocol StubObjectProtocol {

    // MARK: - Functions
    func validate(req: URLRequest) -> Bool
    func response(for req: URLRequest) -> StubResponse
}

struct StubObject: StubObjectProtocol {

    // MARK: - Type
    typealias StubURLValidatorType = ((URLRequest) -> Bool)
    typealias StubRequestValidatorType = ((_ queryParams: [String: String], _ body: [String: Any]) -> Bool)
    typealias StubResponseHandler = ((_ path: String?, _ queryParams: [String: String], _ body: [String: Any]) -> StubResponse)

    // MARK: - Properties
    private var urlValidator: StubURLValidatorType
    private var requestValidator: StubRequestValidatorType?
    private var responseHandler: StubResponseHandler

    // MARK: - Birth and Death
    init(request: StubRequest,
         response: StubResponse,
         urlValidator: StubURLValidatorType? = nil,
         requestValidator: StubRequestValidatorType? = nil) {
        self.requestValidator = requestValidator

        if let urlValidator = urlValidator {
            self.urlValidator = urlValidator
        } else {
            self.urlValidator = { req in
                guard let url = req.url else {
                    return false
                }
                return url.host == request.host && url.path == request.path && req.httpMethod == request.method.rawValue
            }
        }

        self.responseHandler = { _, _, _ in
            return response
        }
    }

    init(request: StubRequest,
         urlValidator: StubURLValidatorType? = nil,
         requestValidator: StubRequestValidatorType? = nil,
         responseHandler: @escaping StubResponseHandler) {

        self.requestValidator = requestValidator

        if let urlValidator = urlValidator {
            self.urlValidator = urlValidator
        } else {
            self.urlValidator = { req in
                guard let url = req.url else {
                    return false
                }
                return url.host == request.host && url.path == request.path && req.httpMethod == request.method.rawValue
            }
        }

        self.responseHandler = responseHandler
    }

    init(urlValidator: @escaping StubURLValidatorType,
         responseHandler: @escaping StubResponseHandler) {
        self.urlValidator = urlValidator
        self.responseHandler = responseHandler
        self.requestValidator = nil
    }

    // MARK: - Validator
    func validate(req: URLRequest) -> Bool {
        let isURLValid = urlValidator(req)

        let queryParams = req.queryParams
        let body = req.body

        let isRequestValid = requestValidator?(queryParams, body) ?? true

        return isURLValid && isRequestValid
    }

    func response(for req: URLRequest) -> StubResponse {
        let queryParams = req.queryParams
        let body = req.body

        return responseHandler(req.url?.path, queryParams, body)
    }
}

private extension URLRequest {

    var queryParams: [String: String] {
        var queryParams: [String: String] = [String: String]()

        if let url = self.url,
            let components = URLComponents(url: url, resolvingAgainstBaseURL: false),
            let queryItems = components.queryItems {
            for queryItem in queryItems {
                queryParams[queryItem.name] = queryItem.value
            }
        }

        return queryParams
    }

    var body: [String: Any] {
        let body: [String: Any]
        if let data = self.ohhttpStubs_httpBody {
            do {
                body = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] ?? [String: Any]()
            } catch let error {
                debugPrint("Error: \(error)")
                body = [String: Any]()
            }
        } else {
            body = [String: Any]()
        }

        return body
    }
}
