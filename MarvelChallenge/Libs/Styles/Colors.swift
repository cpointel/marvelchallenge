//
//  Colors.swift
//  Give
//
//  Created by Cedric Pointel on 18/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import Foundation
import UIKit

internal enum RawColor {

    /// #FFFFFF | 255, 255, 255
    case white

    /// #111111 | 17, 17, 17
    case black

    /// #666666 | 102, 102, 102
    case grey

    /// #999999 | 153, 153, 153
    case greyMedium

    /// #C9CBCB | 201, 203, 203
    case greyLight

    /// #336633 | 51, 102, 51
    case green

    /// #A44A3F | 164, 74, 63
    case brown

    /// #9E273A | 158, 39, 58
    case red

    public var color: UIColor {
        let components: (CGFloat, CGFloat, CGFloat)
        switch self {
        case .white: components = (255, 255, 255)
        case .black: components = (17, 17, 17)
        case .grey: components = (102, 102, 102)
        case .greyMedium: components = (153, 153, 153)
        case .greyLight: components = (201, 203, 203)
        case .green: components = (51, 102, 51)
        case .brown: components = (164, 74, 63)
        case .red: components = (158, 39, 58)
        }
        return UIColor(red: components.0 / 255,
                       green: components.1 / 255,
                       blue: components.2 / 255,
                       alpha: 1)
    }
}

internal struct Color {

    // MARK: - Properties
    public var color: UIColor

    // MARK: - Birth and Death
    private init(uiColor: UIColor, alpha: CGFloat) {
        self.color = uiColor.withAlphaComponent(alpha)
    }

    /**
     */
    static var clear: Color {
        return Color(uiColor: RawColor.black.color, alpha: 0)
    }

    /**
     */
    static func raw(_ rawColor: RawColor, alpha: CGFloat = 1) -> Color {
        return Color(uiColor: rawColor.color, alpha: alpha)
    }

    /**
     */
    static func app(_ style: AppColorStyle, alpha: CGFloat = 1) -> Color {
        return Color(uiColor: style.color, alpha: alpha)
    }
}

internal enum AppColorStyle {

    // Default
    case primary

    // Label
    case text
    case secondaryText
    case tertiaryText

    // Backgrounds
    case background
    case safariBackground

    // CTA
    case ctaPrimaryBackground
    case ctaPrimaryText
    case ctaSecondaryBackground
    case ctaSecondaryText

    // Others
    case separator

    fileprivate var color: UIColor {
        switch self {
        // Default
        case .primary: return RawColor.green.color

        // Label
        case .text: return RawColor.white.color
        case .secondaryText: return RawColor.grey.color
        case .tertiaryText: return RawColor.greyMedium.color

        // Backgrounds
        case .background: return RawColor.black.color
        case .safariBackground: return RawColor.white.color

        // CTA
        case .ctaPrimaryBackground: return RawColor.green.color
        case .ctaPrimaryText: return RawColor.white.color
        case .ctaSecondaryBackground: return RawColor.white.color
        case .ctaSecondaryText: return RawColor.green.color

        // Others
        case .separator: return RawColor.green.color
        }
    }
}
