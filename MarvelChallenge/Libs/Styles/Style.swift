//
//  Styles.swift
//  Give
//
//  Created by Cedric Pointel on 18/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import Foundation
import UIKit

public struct Style<T> {
    typealias Block = (T) -> Void
    public typealias StyleBlock = Style<T>

    private let blocks: [Block]

    public init() {
        self.blocks = []
    }
    internal init(_ block: @escaping Block) {
        self.blocks = [block]
    }
    private init(_ styles: [Style<T>]) {
        self.blocks = styles.flatMap { $0.blocks }
    }

    internal func apply(on item: T) {
        blocks.forEach { $0(item) }
    }

    internal func combine(_ style: Style<T>) -> Style<T> {
        return Style([self, style])
    }
}

public protocol Styleable {}
public extension Styleable {
    func apply(style: Style<Self>) {
        style.apply(on: self)
    }
}
public extension Array where Element: Styleable {
    func apply(style: Style<Element>) {
        self.forEach {
            style.apply(on: $0)
        }
    }
}

public extension Style {
    enum AdjustFontSize {
        case disabled
        case minimumScaleFactor(CGFloat)
    }
}

/// An extension to allow every view to be styleable
extension UIView: Styleable {}
