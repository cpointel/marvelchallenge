//
//  UITextField+Styles.swift
//  Give
//
//  Created by Cedric Pointel on 20/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import UIKit

internal extension Style where T: UITextField {

    func font(_ font: Font) -> StyleBlock {
        return combine(Style { $0.font = font.font })
    }

    func textColor(_ color: Color) -> StyleBlock {
        return combine(Style { $0.textColor = color.color })
    }

    func align(_ alignment: NSTextAlignment) -> StyleBlock {
        return combine(Style { $0.textAlignment = alignment })
    }

    func adjustsFontSizeToFitWidth(_ fontSize: CGFloat) -> StyleBlock {
        return combine(Style {
            $0.adjustsFontSizeToFitWidth = true
            $0.minimumFontSize = fontSize
        })
    }
}
