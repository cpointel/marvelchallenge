//
//  UIActivityIndicatorView+Styles.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 23/10/2020.
//

import UIKit

internal extension Style where T: UIActivityIndicatorView {

    func color(_ color: Color) -> StyleBlock {
        return combine(Style { $0.color = color.color })
    }
}
