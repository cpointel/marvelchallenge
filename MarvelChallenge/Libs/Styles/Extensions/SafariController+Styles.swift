//
//  SafariController.swift
//  Give
//
//  Created by Cedric Pointel on 18/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import SafariServices

extension SFSafariViewController: Styleable {}

internal extension Style where T: SFSafariViewController {

    func barTintColor(_ color: Color) -> StyleBlock {
        return combine(Style { $0.preferredBarTintColor = color.color })
    }

    func controlTintColor(_ color: Color) -> StyleBlock {
        return combine(Style { $0.preferredControlTintColor = color.color })
    }
}
