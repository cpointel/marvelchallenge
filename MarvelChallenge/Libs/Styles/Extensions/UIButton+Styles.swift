//
//  UIButton+Styles.swift
//  Give
//
//  Created by Cedric Pointel on 18/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import UIKit

internal extension Style where T: UIButton {

    func font(_ font: Font) -> StyleBlock {
        return combine(Style { $0.titleLabel?.font = font.font })
    }

    func textColor(_ color: Color, for state: UIControl.State = .normal) -> StyleBlock {
        return combine(Style { $0.setTitleColor(color.color, for: state) })
    }

    func backgroundColor(_ color: Color, for state: UIControl.State = .normal) -> StyleBlock {
        return combine(Style { $0.setBackgroundColor(color.color, for: state) })
    }

    func noBackground() -> StyleBlock {
        return combine(Style { $0.backgroundColor = .clear })
    }

    func edgeInsets(_ edgeInsets: UIEdgeInsets = .zero) -> StyleBlock {
        return combine(Style {
            $0.contentEdgeInsets = edgeInsets
        })
    }
}

private extension UIButton {

    func setBackgroundColor(_ color: UIColor, for state: UIControl.State) {
        self.backgroundColor = .clear
        self.setBackgroundImage(UIImage(color: color), for: state)
    }
}
