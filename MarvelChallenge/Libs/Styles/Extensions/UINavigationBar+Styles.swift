//
//  UINavigationBar+Styles.swift
//  MarvelChallenge
//
//  Created by Cedric Pointel on 25/10/2020.
//

import UIKit

internal extension Style where T: UINavigationBar {

    func barColor(_ color: Color) -> StyleBlock {
        return combine(Style {
            $0.barTintColor = color.color
        })
    }
}
