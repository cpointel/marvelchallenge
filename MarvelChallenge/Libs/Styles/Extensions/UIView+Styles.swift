//
//  UIView+styles.swift
//  Give
//
//  Created by Cedric Pointel on 18/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import UIKit

internal extension Style where T: UIView {

    func backgroundColor(_ color: Color) -> StyleBlock {
        return combine(Style { $0.backgroundColor = color.color })
    }

    func border(color: Color, width: CGFloat) -> StyleBlock {
        return combine(Style {
            $0.layer.borderColor = color.color.cgColor
            $0.layer.borderWidth = width
        })
    }

    func noBorder() -> StyleBlock {
        return combine(Style {
            $0.layer.borderColor = nil
            $0.layer.borderWidth = 0
        })
    }

    func cornerRadius(_ radius: CGFloat) -> StyleBlock {
        return combine(Style {
            $0.layer.masksToBounds = true
            $0.layer.cornerRadius = radius
        })
    }

    func tintColor(_ color: Color) -> StyleBlock {
        return combine(Style { $0.tintColor = color.color })
    }
}
