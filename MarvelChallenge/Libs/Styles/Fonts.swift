//
//  Fonts.swift
//  Give
//
//  Created by Cedric Pointel on 18/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import Foundation
import UIKit

internal enum FontSize {

    /// 10 pts
    case tiny
    /// 12 pts
    case small
    /// 14 pts
    case medium
    /// 16 pts
    case large
    /// 18 pts
    case larger
    /// 30 pts
    case big
    /// 45 pts
    case huge
    /// Others.
    case other(_: CGFloat)

    public var pointSize: CGFloat {
        switch self {
        case .tiny:             return 10
        case .small:            return 12
        case .medium:           return 14
        case .large:            return 16
        case .larger:           return 18
        case .big:              return 30
        case .huge:             return 45
        case .other(let value): return value
        }
    }
}

internal enum Font {

    case light(_: FontSize)
    case regular(_: FontSize)
    case medium(_: FontSize)
    case semiBold(_: FontSize)
    case bold(_: FontSize)

    public var font: UIFont {
        switch self {
        case .light(let size):      return UIFont.systemFont(ofSize: size.pointSize, weight: .light)
        case .regular(let size):    return UIFont.systemFont(ofSize: size.pointSize, weight: .regular)
        case .medium(let size):     return UIFont.systemFont(ofSize: size.pointSize, weight: .medium)
        case .semiBold(let size):   return UIFont.systemFont(ofSize: size.pointSize, weight: .semibold)
        case .bold(let size):       return UIFont.systemFont(ofSize: size.pointSize, weight: .bold)
        }
    }
}
