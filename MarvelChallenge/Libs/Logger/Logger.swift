//
//  Logger.swift
//  Give
//
//  Created by Cedric Pointel on 23/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import Foundation

internal enum LoggerTopic: CaseIterable, CustomStringConvertible {
    case application
    case network
    case navigation

    var description: String {
        switch self {
        case .application: return "App"
        case .network: return "Network"
        case .navigation: return "Navigation"
        }
    }
}

internal enum LoggerLevel: Int, CustomStringConvertible {

    case off = 0
    case fatal
    case error
    case warning
    case info
    case debug

    var description: String {
        switch self {
        case .off: return "Off"
        case .debug: return "Debug"
        case .info: return "Info"
        case .warning: return "Warn"
        case .error: return "Error"
        case .fatal: return "Fatal"
        }
    }
}

internal enum LoggerFormat {
    /// \(level)\(topic) \(date) - \(message) \(extra?)
    case common
    /// \(picto)\(topic) \(date) [\(file).\(function).l\(line)] - \(message) \(extra?)
    case dev
    /// \(picto)\(topic) - \(message) \(extra?)
    case short
    /// \(message) \(extra?)
    case tiny
}

internal class Logger {

    static let timestampFormat: String = "yyyy-MM-dd HH:mm:ss.SSS"

    // MARK: - Type
    typealias LoggerType = (String) -> Void

    // MARK: - Properties
    private let logger: LoggerType
    private let generalLevel: LoggerLevel
    private let generalFormat: LoggerFormat
    private let topicLevels: [LoggerTopic: LoggerLevel]
    private let topicFormats: [LoggerTopic: LoggerFormat]
    private let dateFormatter = DateFormatter()

    // MARK: - Birth and Death
    init(logger: @escaping LoggerType,
         level: LoggerLevel = .off,
         format: LoggerFormat = .common,
         topicLevels: [LoggerTopic: LoggerLevel] = [:],
         topicFormats: [LoggerTopic: LoggerFormat] = [:]) {
        self.logger = logger
        self.generalLevel = level
        self.generalFormat = format
        self.topicLevels = topicLevels
        self.topicFormats = topicFormats

        dateFormatter.dateFormat = Logger.timestampFormat
    }

    private func shouldDisplayLog(for topic: LoggerTopic?, level: LoggerLevel) -> Bool {
        let topicLevel = topic.flatMap { topicLevels[$0] }
        let validLevel = topicLevel ?? generalLevel
        return level.rawValue <= validLevel.rawValue && level.rawValue <= generalLevel.rawValue
    }

    private func timestamp() -> String {
        return dateFormatter.string(from: Date())
    }

    private func format(message: String, for topic: LoggerTopic?, level: LoggerLevel, file: String, line: Int, function: String) -> String {
        let picto = indicator(level: level)
        let date = timestamp()
        let topic = topic != nil ? "[\(topic?.description ?? "")]" : ""
        let file = ((file as NSString).lastPathComponent as NSString).deletingPathExtension

        return "\(picto)\(topic) \(date) [\(file).\(function).l\(line)] - \(message)"
    }

}

// MARK: - Logger / Logs
extension Logger {

    private func log(_ message: String, for topic: LoggerTopic?, level: LoggerLevel, file: String?, line: Int?, function: String?, extra: String? = nil) {

        if shouldDisplayLog(for: topic, level: level) {
            let topicFormat = topic.flatMap { topicFormats[$0] }
            let format = topicFormat ?? generalFormat

            let log: String

            switch format {
            case .common:
                log = formatCommon(message: message, for: topic, level: level, extra: extra)
            case .dev:
                log = formatDev(message: message, for: topic, level: level, file: file, line: line, function: function, extra: extra)
            case .short:
                log = formatShort(message: message, for: topic, level: level, extra: extra)
            case .tiny:
                log = formatTiny(message: message, extra: extra)
            }

            logger(log)
        }
    }

    func logDebug(_ message: String, for topic: LoggerTopic? = nil, file: String = #file, line: Int = #line, function: String = #function) {
        log(message, for: topic, level: .debug, file: file, line: line, function: function)
    }

    func logInfo(_ message: String, for topic: LoggerTopic? = nil, file: String = #file, line: Int = #line, function: String = #function) {
        log(message, for: topic, level: .info, file: file, line: line, function: function)
    }

    func logWarning(_ message: String, for topic: LoggerTopic? = nil, file: String = #file, line: Int = #line, function: String = #function) {
        log(message, for: topic, level: .warning, file: file, line: line, function: function)
    }

    func logError(_ message: String, for topic: LoggerTopic? = nil, file: String = #file, line: Int = #line, function: String = #function) {
        log(message, for: topic, level: .error, file: file, line: line, function: function)
    }

    func logFatal(_ message: String, for topic: LoggerTopic? = nil, file: String = #file, line: Int = #line, function: String = #function) {
        log(message, for: topic, level: .fatal, file: file, line: line, function: function)
    }
}

// MARK: - Logger / Format
extension Logger {

    private func formatCommon(message: String, for topic: LoggerTopic?, level: LoggerLevel, extra: String? = nil) -> String {
        let level = formatLevel(level)
        let topic = formatTopic(topic)
        let date = timestamp()

        var message = "\(level)\(topic) \(date) - \(message)"
        if let extra = extra { message += " " + extra }
        return message
    }

    private func formatDev(message: String, for topic: LoggerTopic?, level: LoggerLevel, file: String?, line: Int?, function: String?, extra: String? = nil) -> String {
        let picto = indicator(level: level)
        let topic = formatTopic(topic)
        let date = timestamp()
        var code: String?
        if let file = file, let line = line, let function = function {
            let file = formatFile(file)
            code = "[\(file).\(function).l\(line)]"
        }

        // Build the message
        var display = "\(picto)\(topic) \(date)"
        if let code = code { display += " \(code)" }
        display += " - \(message)"
        if let extra = extra { display += " \(extra)" }

        return display
    }

    private func formatShort(message: String, for topic: LoggerTopic?, level: LoggerLevel, extra: String? = nil) -> String {
        let picto = indicator(level: level)
        let topic = formatTopic(topic)
        var message = "\(picto)\(topic) - \(message)"
        if let extra = extra { message += " " + extra }
        return message
    }

    //
    private func formatTiny(message: String, extra: String? = nil) -> String {
        var message = "\(message)"
        if let extra = extra { message += " " + extra }
        return message
    }

    private func indicator(level: LoggerLevel) -> String {
        switch level {
        case .off: return ""
        case .debug: return "⚙️"
        case .info: return "ℹ️"
        case .warning: return "⚠️"
        case .error: return "🔥"
        case .fatal: return "☄️"
        }
    }

    private func formatTopic(_ topic: LoggerTopic?) -> String {
        return topic != nil ? "[\(topic?.description ?? "")]" : ""
    }

    private func formatLevel(_ level: LoggerLevel) -> String {
        return level.description
    }

    private func formatFile(_ file: String) -> String {
        return ((file as NSString).lastPathComponent as NSString).deletingPathExtension
    }
}

// MARK: - Default Logger
extension Logger {

    static func logDebug(_ message: String, for topic: LoggerTopic? = nil, file: String = #file, line: Int = #line, function: String = #function) {
        defaultLogger.logDebug(message, for: topic, file: file, line: line, function: function)
    }

    static func logInfo(_ message: String, for topic: LoggerTopic? = nil, file: String = #file, line: Int = #line, function: String = #function) {
        defaultLogger.logInfo(message, for: topic, file: file, line: line, function: function)
    }

    static func logWarning(_ message: String, for topic: LoggerTopic? = nil, file: String = #file, line: Int = #line, function: String = #function) {
        defaultLogger.logWarning(message, for: topic, file: file, line: line, function: function)
    }

    static func logError(_ message: String, for topic: LoggerTopic? = nil, file: String = #file, line: Int = #line, function: String = #function) {
        defaultLogger.logError(message, for: topic, file: file, line: line, function: function)
    }

    static func logFatal(_ message: String, for topic: LoggerTopic? = nil, file: String = #file, line: Int = #line, function: String = #function) {
        defaultLogger.logFatal(message, for: topic, file: file, line: line, function: function)
    }
}
