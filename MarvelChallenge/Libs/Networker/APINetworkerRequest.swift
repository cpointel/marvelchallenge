//
//  APINetworkerRequest.swift
//  Give
//
//  Created by Cedric Pointel on 22/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import Foundation

class APINetworkerRequest {

    public typealias Path = String
    public typealias Parameters = [String: Any]
    public typealias Headers = [String: String]

    // MARK: - Enums
    enum Method {
      case get
      case post
      case put
      case delete
    }

    enum ParameterEncoding {
      case json
      case urlEncoded
    }

    // MARK: - Properties
    let method: APINetworkerRequest.Method
    let path: Path
    let headers: Headers
    let parameters: Parameters
    let encoding: APINetworkerRequest.ParameterEncoding

    // MARK: - Birth and Death
    init(method: APINetworkerRequest.Method = APINetworkerRequest.Method.get,
         path: APINetworkerRequest.Path,
         headers: APINetworkerRequest.Headers = [:],
         parameters: APINetworkerRequest.Parameters = [:],
         encoding: APINetworkerRequest.ParameterEncoding = APINetworkerRequest.ParameterEncoding.json) {
        self.method = method
        self.path = path
        self.headers = headers
        self.parameters = parameters

        if method == .get {
            self.encoding = .urlEncoded
        } else {
            self.encoding = encoding
        }
    }
}
