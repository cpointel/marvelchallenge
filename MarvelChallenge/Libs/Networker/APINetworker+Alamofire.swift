//
//  APINetworkerRequest+Alamofire.swift
//  Give
//
//  Created by Cedric Pointel on 22/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import Foundation
import Alamofire

extension APINetworker {

    static func afNetworker(url: URL) -> APINetworker? {
        return APINetworker(url: url, requester: SessionManager())
    }

    static func afNetworker(scheme: String = "https", host: String) -> APINetworker {
        return APINetworker(scheme: scheme, host: host, requester: SessionManager())
    }
}

extension SessionManager: APINetworkerRequesterProtocol {

    func request(_ url: URL,
                 method: APINetworkerRequest.Method,
                 parameters: APINetworkerRequest.Parameters,
                 encoding: APINetworkerRequest.ParameterEncoding,
                 headers: APINetworkerRequest.Headers,
                 handler: @escaping APINetworkkerRequesterHandler) {

        request(url,
                method: method.alamofire,
                parameters: parameters,
                encoding: encoding.alamofire,
                headers: headers)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                switch response.result {
                case .success:
                    handler(response.data, nil)
                case .failure(let error):
                    handler(response.data, error)
                }
            }
    }
}

extension APINetworkerRequest.Method {
    var alamofire: Alamofire.HTTPMethod {
        switch self {
        case .get: return Alamofire.HTTPMethod.get
        case .post: return Alamofire.HTTPMethod.post
        case .put: return Alamofire.HTTPMethod.put
        case .delete: return Alamofire.HTTPMethod.delete
        }
    }
}

extension APINetworkerRequest.ParameterEncoding {
    var alamofire: Alamofire.ParameterEncoding {
        switch self {
        case .json: return JSONEncoding.default
        case .urlEncoded: return URLEncoding.default
        }
    }
}
