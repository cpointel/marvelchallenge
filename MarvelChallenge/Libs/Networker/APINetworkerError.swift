//
//  APINetworkerError.swift
//  Give
//
//  Created by Cedric Pointel on 23/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import Foundation
import Alamofire

internal enum APINetworkerError: Error {

    /// 400..500
    case badRequest     // 400
    case unauthorized   // 401
    case forbidden      // 403
    /// 500..600
    case internalServerError
    /// Others
    case noNetwork
    case noData
    case unreachableServer
    case invalidURL
    case parsingError(error: Error)
    case unacceptableStatusCode
    case other(error: Error)

    // MARK: - Birth and Death
    // swiftlint:disable cyclomatic_complexity
    init(error: Error) {
        if let error = error as? APINetworkerError {
            self = error
        } else if let urlError = error as? URLError {
            switch urlError {
            case URLError.notConnectedToInternet,
                 URLError.networkConnectionLost:
                self = .noNetwork
            case URLError.userAuthenticationRequired:
                self = .unauthorized
            case URLError.cannotConnectToHost,
                 URLError.cannotFindHost:
                self = .unreachableServer
            case URLError.cannotParseResponse:
                self = .parsingError(error: error)
            default:
                self = .other(error: error)
            }
        } else if let afError = error as? AFError {
            switch afError {
            case AFError.responseSerializationFailed:
                self = .parsingError(error: error)
            case AFError.responseValidationFailed(let reason):
                switch reason {
                case .unacceptableStatusCode(let code):
                    if (500..<600).contains(code) {
                        self = .internalServerError
                    } else if code == 400 {
                        self = .badRequest
                    } else if code == 401 {
                        self = .unauthorized
                    } else if code == 403 {
                        self = .forbidden
                    } else {
                        self = .unacceptableStatusCode
                    }
                default:
                    self = .other(error: error)
                }
            default:
                self = .other(error: error)
            }
        } else {
            self = .other(error: error)
        }
    }

    var localizedDescription: String {
        switch self {
        case .badRequest: return "badRequest"
        case .unauthorized: return "unauthorized"
        case .forbidden: return "forbidden"
        case .internalServerError: return "internalServerError"
        case .noNetwork: return "noNetwork"
        case .noData: return "noData"
        case .unreachableServer: return "unreachableServer"
        case .invalidURL: return "invalidURL"
        case .parsingError(let error): return "parsingError: \(error.localizedDescription)"
        case .unacceptableStatusCode: return "unacceptableStatusCode"
        case .other(let error): return "other: \(error.localizedDescription)"
        }
    }
}
