//
//  APINetworker.swift
//  Give
//
//  Created by Cedric Pointel on 22/04/2020.
//  Copyright © 2020 cpo. All rights reserved.
//

import Foundation
import RxSwift

protocol APINetworkerRequesterProtocol {

    typealias APINetworkkerRequesterHandler = ((Data?, Error?) -> Void)

    func request(_ url: URL,
                 method: APINetworkerRequest.Method,
                 parameters: APINetworkerRequest.Parameters,
                 encoding: APINetworkerRequest.ParameterEncoding,
                 headers: APINetworkerRequest.Headers,
                 handler: @escaping APINetworkkerRequesterHandler)
}

protocol APINetworkerProtocol {

    // MARK: - Functions
    func execute<T: Decodable>(request: APINetworkerRequest) -> Observable<T>
}

class APINetworker: APINetworkerProtocol {

    // MARK: - Properties
    let scheme: String
    let host: String
    let requester: APINetworkerRequesterProtocol

    // MARK: - Birth and Death
    convenience init?(url: URL, requester: APINetworkerRequesterProtocol) {
        guard let scheme = url.scheme else { return nil }
        guard let host = url.host else { return nil }

        self.init(scheme: scheme, host: host, requester: requester)
    }

    init(scheme: String = "https", host: String, requester: APINetworkerRequesterProtocol) {
        Logger.logInfo("Init Networker for '\(scheme)://\(host)'", for: .application)
        self.scheme = scheme
        self.host = host
        self.requester = requester
    }

    private func constructUrl(request: APINetworkerRequest) -> URL? {
      return URL(string: "\(scheme)://\(host)\(request.path)")
    }
}

// MARK: APINetworkerProtocol / Functions
extension APINetworker {

    // swiftlint:disable cyclomatic_complexity
    func execute<T: Decodable>(request: APINetworkerRequest) -> Observable<T> {
        guard let url = constructUrl(request: request) else {
            Logger.logError("Can't generate url for request at path \(request.path)", for: .network)
            return .error(RxError.unknown)
        }

        return Observable.create { [weak self] observer in
            guard let strongSelf = self else {
                return Disposables.create()
            }

            let uniqueIdentifier = NSUUID()
            let handler: APINetworkerRequesterProtocol.APINetworkkerRequesterHandler = { data, error in

                Logger.logInfo("Response received for \(url) [\(uniqueIdentifier)]", for: .network)
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    Logger.logDebug("with data:\n\(dataString)", for: .network)
                }

                if let error = error {
                    let error = APINetworkerError(error: error)
                    Logger.logError("with error '\(error.localizedDescription)'", for: .network)
                    observer.onError(error)
                } else if let data = data {
                    do {
                        let object = try JSONDecoder().decode(T.self, from: data)
                        observer.onNext(object)
                        observer.onCompleted()
                    } catch let error {
                        let error = APINetworkerError.parsingError(error: error)
                        Logger.logError("with error '\(error.localizedDescription)'", for: .network)
                        observer.onError(error)
                    }
                } else {
                    let error = APINetworkerError.noData
                    Logger.logError("with error '\(error.localizedDescription)'", for: .network)
                    observer.onError(error)
                }
            }

            Logger.logInfo("\("\(request.method)".uppercased()) request at \(url) [\(uniqueIdentifier)]", for: .network)
            if request.method == .get {
                var queryParams = ""
                for object in request.parameters.enumerated() {
                    queryParams += "\n\(object.element.key) = \(object.element.value)"
                }
                if !request.parameters.isEmpty { Logger.logInfo("queryParams: \(queryParams)", for: .network) }
            }
            if !request.headers.isEmpty { Logger.logDebug("with headers \(request.headers)", for: .network) }
            if request.method != .get {
                if !request.parameters.isEmpty { Logger.logDebug("with body \(request.parameters)", for: .network) }
            }

            strongSelf.requester.request(url,
                                         method: request.method,
                                         parameters: request.parameters,
                                         encoding: request.encoding,
                                         headers: request.headers,
                                         handler: handler)

            return Disposables.create()
        }.observeOn(SerialDispatchQueueScheduler(qos: .background))
    }
}
