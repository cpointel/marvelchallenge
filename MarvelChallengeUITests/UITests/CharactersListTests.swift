//
//  CharactersListTests.swift
//  MarvelChallengeUITests
//
//  Created by Cedric Pointel on 25/10/2020.
//

import XCTest

class CharactersListTests: XCTestCase {

    // MARK: - Properties
    let applicationSteps = ApplicationSteps()
    let charactersListSteps = CharactersListSteps()

    // MARK: - Override
    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    override func tearDownWithError() throws {}
}

extension CharactersListTests {

    func test_charactersScreen() {
        Given("I launch the application", step: applicationSteps.iLaunchTheApplication)
        Then("I am on 'Characters' screen", step: charactersListSteps.iSeeCharactersListPage)
        And("I see all characters view", step: charactersListSteps.iSeeResultsView)
    }

    func test_charactersNoResultView() {
        Given("I launch the application", step: applicationSteps.iLaunchTheApplication)
        Then("I am on 'Characters' screen", step: charactersListSteps.iSeeCharactersListPage)
        When("I type 'abcd' in searchBar", step: charactersListSteps.iFillSearchBar("abcd"))
        Then("I see no result view", step: charactersListSteps.iSeeNoResultView)
    }

    func test_charactersResultsView() {
        Given("I launch the application", step: applicationSteps.iLaunchTheApplication)
        Then("I am on 'Characters' screen", step: charactersListSteps.iSeeCharactersListPage)
        When("I type 'iron' in searchBar", step: charactersListSteps.iFillSearchBar("iron"))
        Then("I see results view", step: charactersListSteps.iSeeResultsView)
    }
}
