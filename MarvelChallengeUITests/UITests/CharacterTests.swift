//
//  CharacterTests.swift
//  MarvelChallengeUITests
//
//  Created by Cedric Pointel on 27/10/2020.
//

import XCTest

class CharacterTests: XCTestCase {

    // MARK: - Properties
    let applicationSteps = ApplicationSteps()
    let charactersListSteps = CharactersListSteps()
    let characterSteps = CharacterSteps()

    // MARK: - Override
    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    override func tearDownWithError() throws {}
}

extension CharacterTests {

    func test_characterScreenWithOnlyComics() {
        Given("I launch the application", step: applicationSteps.iLaunchTheApplication)
        When("I select first item", step: charactersListSteps.tapOnResult(at: 0))
        Then("I see character view", step: characterSteps.iSeeCharacterPage)
        And("I don't see description view", step: characterSteps.iCantSeeDescriptionView)
        And("I see comics view", step: characterSteps.iSeeComicsView)
    }

    func test_characterScreenWithOnlyDescription() {
        Given("I launch the application", step: applicationSteps.iLaunchTheApplication)
        When("I select second item", step: charactersListSteps.tapOnResult(at: 1))
        Then("I see character view", step: characterSteps.iSeeCharacterPage)
        And("I see description view", step: characterSteps.iSeeDescriptionView)
        And("I don't see comics view", step: characterSteps.iCantSeeComicsView)
    }

    func test_characterScreenWithDescriptionAndComics() {
        Given("I launch the application", step: applicationSteps.iLaunchTheApplication)
        When("I select third item", step: charactersListSteps.tapOnResult(at: 2))
        Then("I see character view", step: characterSteps.iSeeCharacterPage)
        And("I see description view", step: characterSteps.iSeeDescriptionView)
        And("I see comics view", step: characterSteps.iSeeComicsView)
    }
}
