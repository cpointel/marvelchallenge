//
//  Application.swift
//  MarvelChallengeUITests
//
//  Created by Cedric Pointel on 25/10/2020.
//

import XCTest

let app = Application.shared.xcApp

class Application {

    static let shared = Application()

    // MARK: - Properties
    let xcApp = XCUIApplication()

    // MARK: - Birth and Death
    private init() {}

    // MARK: - Actions
    func launch() {
        xcApp.launch()
    }
}
