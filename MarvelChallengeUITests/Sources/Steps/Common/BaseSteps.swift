//
//  BaseSteps.swift
//  MarvelChallengeUITests
//
//  Created by Cedric Pointel on 25/10/2020.
//

import XCTest

private let timeout: TimeInterval = 30

class BaseSteps {

    // MARK: - Properties
    private var currentTestCase = XCTestCase()
}

// MARK: - Actions
extension BaseSteps {

    func tapOn(locator: XCUIElement, file: String = #file, line: UInt = #line) {
        waitForElementToAppear(locator: locator, file: file, line: line)
        locator.tap()
    }

    func typeText(locator: XCUIElement, text: String, file: String = #file, line: UInt = #line) {
        let result = isDisplayed(locator: locator, file: file, line: line)
        if result {
            locator.typeText(text)
        } else {
            XCTFail("Type text failed for \(locator)")
        }
    }
}

// MARK: - Wait
extension BaseSteps {

    @discardableResult
    private func waitForElementToAppear(locator: XCUIElement, file: String, line: UInt) -> Bool {
        return currentTestCase.waitUntil(locator, timeout, file: file, line: line, is: .exists)
    }

    @discardableResult
    private func waitForElementToDisappear(locator: XCUIElement, file: String, line: UInt) -> Bool {
        return currentTestCase.waitUntil(locator, timeout, file: file, line: line, is: .doesNotExist)
    }
}

// MARK: - Asserts
extension BaseSteps {

    func isDisplayed(locator: XCUIElement, file: String = #file, line: UInt = #line) -> Bool {
        return waitForElementToAppear(locator: locator, file: file, line: line)
    }

    func isHidden(locator: XCUIElement, file: String = #file, line: UInt = #line) -> Bool {
        return waitForElementToDisappear(locator: locator, file: file, line: line)
    }

}
