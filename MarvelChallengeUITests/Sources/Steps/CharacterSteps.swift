//
//  CharacterSteps.swift
//  MarvelChallengeUITests
//
//  Created by Cedric Pointel on 27/10/2020.
//

import XCTest

class CharacterSteps: BaseSteps {

    // MARK: - Properties
    private let characterScreen = CharacterScreen()
}

// MARK: - Actions
extension CharacterSteps {}

// MARK: - Asserts
extension CharacterSteps {

    func iSeeCharacterPage() {
        XCTAssertTrue(isDisplayed(locator: characterScreen.page))
    }

    func iCantSeeDescriptionView() {
        XCTAssertTrue(isHidden(locator: characterScreen.descriptionView))
    }

    func iSeeDescriptionView() {
        XCTAssertTrue(isDisplayed(locator: characterScreen.descriptionView))
    }

    func iCantSeeComicsView() {
        XCTAssertTrue(isHidden(locator: characterScreen.comicsView))
    }

    func iSeeComicsView() {
        XCTAssertTrue(isDisplayed(locator: characterScreen.comicsView))
    }
}
