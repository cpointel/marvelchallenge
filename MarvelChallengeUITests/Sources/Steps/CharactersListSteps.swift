//
//  CharactersListSteps.swift
//  MarvelChallengeUITests
//
//  Created by Cedric Pointel on 25/10/2020.
//

import XCTest

class CharactersListSteps: BaseSteps {

    // MARK: - Properties
    private let charactersListScreen = CharactersListScreen()
}

// MARK: - Actions
extension CharactersListSteps {

    func iFillSearchBar(_ text: String = "") {
        let searchBar = charactersListScreen.searchBar
        tapOn(locator: searchBar)
        typeText(locator: searchBar, text: text)
    }

    func tapOnResult(at index: Int) {
        tapOn(locator: charactersListScreen.cell(cellIndex: index))
    }
}

// MARK: - Asserts
extension CharactersListSteps {

    func iSeeCharactersListPage() {
        XCTAssertTrue(isDisplayed(locator: charactersListScreen.page))
    }

    func iSeeNoResultView() {
        XCTAssertTrue(isDisplayed(locator: charactersListScreen.noResultView))
    }

    func iSeeResultsView() {
        XCTAssertTrue(isDisplayed(locator: charactersListScreen.resultsView))
    }
}
