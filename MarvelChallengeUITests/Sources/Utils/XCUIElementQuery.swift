//
//  XCUIElementQuery.swift
//  MarvelChallengeUITests
//
//  Created by Cedric Pointel on 25/10/2020.
//

import XCTest

extension XCUIElementQuery {

    subscript(accessibility: AccessibilityIdentifiers) -> XCUIElement {
        return self[accessibility.rawValue]
    }

    func containing(_ elementType: XCUIElement.ElementType, accessibility: AccessibilityIdentifiers) -> XCUIElementQuery {
        return containing(elementType, identifier: accessibility.rawValue)
    }

    func matching(accessibility: AccessibilityIdentifiers) -> XCUIElementQuery {
        return matching(identifier: accessibility.rawValue)
    }

}
