//
//  BDDSteps.swift
//  MarvelChallengeUITests
//
//  Created by Cedric Pointel on 25/10/2020.
//

import XCTest

extension XCTest {

    func Given(_ text: String, step: () -> Void) {
        XCTContext.runActivity(named: #function + " " + text) { _ in
            step()
        }
    }

    func When(_ text: String, step: () -> Void) {
        XCTContext.runActivity(named: #function + " " + text) { _ in
            step()
        }
    }

    func Then(_ text: String, step: () -> Void) {
        XCTContext.runActivity(named: #function + " " + text) { _ in
            step()
        }
    }

    func And(_ text: String, step: () -> Void) {
        XCTContext.runActivity(named: #function + " " + text) { _ in
            step()
        }
    }

    func Given(_ text: String, step: @autoclosure () -> Void) {
        XCTContext.runActivity(named: #function + " " + text) { _ in
            step()
        }
    }

    func When(_ text: String, step: @autoclosure () -> Void) {
        XCTContext.runActivity(named: #function + " " + text) { _ in
            step()
        }
    }

    func Then(_ text: String, step: @autoclosure () -> Void) {
        XCTContext.runActivity(named: #function + " " + text) { _ in
            step()
        }
    }

    func And(_ text: String, step: @autoclosure () -> Void) {
        XCTContext.runActivity(named: #function + " " + text) { _ in
            step()
        }
    }
}
