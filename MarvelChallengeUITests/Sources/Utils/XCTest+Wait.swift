//
//  XCTest+Wait.swift
//  MarvelChallengeUITests
//
//  Created by Cedric Pointel on 25/10/2020.
//

import XCTest

enum XCWaitPredicate: String {
    case exists = "exists == true"
    case doesNotExist = "self.exists == false"
    case selected = "isSelected == true"
    case hittable = "isHittable == true"
    case notHittable = "isHittable == false"
    case enabled = "isEnabled == true"
}

extension XCTestCase {

    func waitUntil(_ element: XCUIElement,
                   _ timeout: TimeInterval = 15,
                   file: String,
                   line: UInt,
                   `is` conditions: XCWaitPredicate ...) -> Bool {
        let predicate = NSPredicate(format: conditions.map { $0.rawValue }.joined(separator: " AND "))
        let expectation = self.expectation(for: predicate, evaluatedWith: element, handler: nil)
        let result = XCTWaiter().wait(for: [expectation], timeout: timeout)
        switch result {
        case .completed:
            return true
        default:
            let issue = XCTIssue(type: .assertionFailure,
                                 compactDescription: "Conditions \(predicate) failed for \(element) after \(timeout) seconds.\nfile: \(file) | line: \(line)")
            record(issue)
            return false
        }
    }
}
