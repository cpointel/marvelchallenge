//
//  CharactersListScreen.swift
//  MarvelChallengeUITests
//
//  Created by Cedric Pointel on 25/10/2020.
//

import XCTest

class CharactersListScreen {

    // MARK: - Properties
    let page = app.otherElements[.sceneCharactersListScreenPage]
    let searchBar = app.otherElements[.sceneCharactersListSearchBar]
    let noResultView = app.otherElements[.sceneCharactersListNoResultPlaceholderView]
    let resultsView = app.collectionViews[.sceneCharactersListResultsView]

    func cell(cellIndex: Int) -> XCUIElement {
        return resultsView.cells.element(boundBy: cellIndex)
    }
}
