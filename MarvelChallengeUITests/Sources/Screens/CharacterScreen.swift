//
//  CharacterScreen.swift
//  MarvelChallengeUITests
//
//  Created by Cedric Pointel on 27/10/2020.
//

import XCTest

class CharacterScreen {

    // MARK: - Properties
    let page = app.otherElements[.sceneCharacterScreenPage]
    let descriptionView = app.otherElements[.sceneCharacterDescriptionView]
    let comicsView = app.otherElements[.sceneCharacterComicsView]
}
