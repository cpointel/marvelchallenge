#  Marvel Challenge

## Introduction

### Summary

Write an iOS/Android/Web app that serves as a wiki of Marvel characters. 
This challenge was designed to be something the right candidate can put together in a few hours. 
You’ll have one week to complete it. Since we know that life can be sometimes unpredictable, please do let us know if you need a deadline extension.

### Functional requirements

The app should have the following functionalities:

* List all Marvel characters (we suggest displaying this as items on a UICollectionView. Each cell should include both the character name and a picture for that character)
* Allow the user to filter the list based on the character’s name
* Display the details of any given character when the user clicks a name on the list

The [Marvel API](https://developer.marvel.com/) should provide you with access to the whole character database.

### Non functional requirements

* The project should be well organised. While we don’t require a specific architectural pattern, please note that anti-patterns such as massive view controllers that contain all the logic are frowned upon.
* You are free to use any frameworks you’d like but you might be required to justify your choice later. We do encourage you to use a reactive programming framework if you are comfortable with that, and SDWebImage can help you to load the remote images from the Marvel API.
* The Marvel API returns a paginated result for the character endpoint. We suggest you deal with this by making the app fetch more results once the users scrolls sufficiently to the bottom of the list.

### Bonus

We consider the following as bonuses:

* Implementing your UI with code (i.e. not using Storyboards or Interface Builder)
* Writing a small test suite
* Using a reactive programming framework

### Delivery

The project should be delivered as a link to a repository hosted on Github & a video. 
Please include on the project’s read-me simple instructions on how to build the project.


## Application

### Screenshots

![text](Resources/Screenshot1_Search.PNG){width=100 height=217}
![text](Resources/Screenshot2_SearchResults.PNG){width=100 height=217}
![text](Resources/Screenshot3_Character.PNG){width=100 height=217}
![text](Resources/Screenshot4_SearchNoResult.PNG){width=100 height=217}

- [Search](https://bitbucket.org/cpointel/marvelchallenge/src/develop/Resources/Screenshot1_Search.PNG)
- [Search with results](https://bitbucket.org/cpointel/marvelchallenge/src/develop/Resources/Screenshot2_SearchResults.PNG)
- [Character page](https://bitbucket.org/cpointel/marvelchallenge/src/develop/Resources/Screenshot3_Character.PNG)
- [Search with no result page](https://bitbucket.org/cpointel/marvelchallenge/src/develop/Resources/Screenshot4_SearchNoResult.PNG)

### Video

You can see a demo [here](https://bitbucket.org/cpointel/marvelchallenge/src/develop/Resources/AppDemo_lowQuality.mp4)

## Dependencies


### Description

The tool 'cocoapods' is used to manage project's dependencies.
Current dependencies are listed below:

* 'RxSwift' & 'RxCocoa' for reactive programming
* 'Alamofire' for networking
* 'AlamofireImage' for images networking and effects
* 'Reusable' for create views with type safe way
* 'Swinject' for dependency injection

* 'SwiftLint' to enforce Swift style and conventions
    * a script for SwiftLint is configured in 'Build Phases' => 'Swiftlint Analysis'
* 'SwiftGen' to automatically generate Swift code for resources of the project (images, localizables). See folder 'Tools/swiftgen.yml' for the configuration.
    * files generated are located in 'Sources/Constants/Generated'
    * a script for SwiftGen is configured in 'Build Phases' => 'SwiftGen Code Generation'
* 'OHHTTPStubs/Swift' to manage stubs/mocks in order to help in development/UITests. See folder 'Resources/Stubs' for fixtures.

* 'RxTest' only for the UnitTests target

### Installation

To install all dependencies:

- `bundle install`
- `pod install`

## Architecture

MVVM.

## Custom Librairies

Some custom libraries have been used for the project.

* 'Logger' for debuging on Console
* 'Networker' a layer on top of d'Alamofire
* 'Styles' for stylization for UI components
* 'Stubs' to make easy to use stubs/mocks & 'OHHTTPStubs/Swift'

## Configuration

There are 2 configurations in the application:

* Debug for development
    * /!\ currently only with stubs/mocks
    * To facilitate the search, you should use 'iron ma'
    * Random response times have been configured to simulate the network
    * The 'Logger' is available only for this configuration
* Release for production with the MarvelAPI

Use of '*.xcconfig' in order to test this way on this project. See 'Resources/Configuration'

## Compatibility

The minimum version of this project is iOS12.

## Unit Tests

The UnitTests target is ready and works.

## UI Tests

The UI Tests target is ready and works.

## Remarks

* There are some '// TODO' in UnitTest target
* A script should be added in 'Build Phases' in order to remove fixtures for release
* There is a debounce of 300ms for the search

* The application has been tested on:
    * Simulator iPhone 11 - iOS 14.0
    * Simulator iPhone 5S - iOS 12.4
    * Device iPhone 11 Pro - iOS 14.0.1

